-- Entité représentant un niveau de jeu
-- Utilise une forme d'héritage simple: player qui hérite de sprite qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent.
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/functions")
require("base/sprite")

--- Crée un pseudo objet de type player
-- @return un pseudo objet player
function newPlayer ()
  -- création d'un objet parent
  local super = newSprite()
  -- création d'un objet enfant
  local self = super:new()

  -- attributs privés accessibles via les getter/setters
  -------------
  -- utilise les touches du clavier pour le déplacement de l'objet
  local moveWithKeys = true
  -- utilise la souris pour le déplacement de l'objet
  local moveWithMouse = false
  -- les touches utilisées pour les déplacements du joueur (propres à chaque joueur)
  local keys = {}
  -- niveau
  local level = true
  -- chemin pour l'image représentant une vie
  local livesImagePath =""
  -- score
  local score = 0
  -- nombre de vies au départ
  local livesStart = 0
  -- nombre de vies restante
  local livesLeft = 0
  -- le joueur a t'il gagné la partie ?
  local hasWon = false
  -- liste des touches de clavier devant être relâchées avant une action
  local keysToRelease = {}
  --- Initialise l'objet
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur 0 sera utilisée.
  -- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur 0 sera utilisée.
  -- @param pWidth (OPTIONNEL) largeur en pixel. Si absent la valeur map.get_tileWidth() sera utilisée.
  -- @param pHeight (OPTIONNEL) hauteur en pixel. Si absent la valeur map.get_tileHeight() sera utilisée.
  -- @param pImage (OPTIONNEL) image principale du sprite.
  -- @param pVelocityJump (OPTIONNEL) vélocité pour un saut. Si absent la valeur 0 sera utilisée.
  -- @param pStartSpeed (OPTIONNEL) vitesse initiale. Si absent la valeur 0 sera utilisée.
  -- @param pFriction (OPTIONNEL) freinage du à l'environnement. Si absent la valeur 0 sera utilisée.
  -- @param pStartGravity (OPTIONNEL) chute due à la gravité. Si absent la valeur 0 sera utilisée.
  -- @param pVelocityMax (OPTIONNEL) vélocité maximale. Si absent la valeur 0 sera utilisée. Si la valeur est 0, l'objet ne pourra pas se déplacer
  -- @param pWeight (OPTIONNEL) poids (influence la gravité et la résistance de l'environnement). Si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage
  -- @param pLiveImagePath (OPTIONNEL) chemin pour l'image représentant une vie. Mettre à "" pour ne pas utiliser d'image pour l'affichage des vies
  -- @param pLevel (OPTIONNEL) niveau. Mettre à -1 pour ne pas afficher la valeur dans le HUD
  -- @param pLivesStart (OPTIONNEL) nombre de vies restante.Mettre à -1 pour ne pas afficher la valeur dans le HUD
  -- @param pScore (OPTIONNEL) score.Mettre à -1 pour ne pas afficher la valeur dans le HUD
  -- @param pMoveWithVelocity (OPTIONNEL) true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique)
  -- @param pMoveWithMouse (OPTIONNEL) true pour utiliser la souris pour le déplacement de l'objet
  -- @param pMoveWithKeys (OPTIONNEL) true pour utiliser les touches du clavier pour le déplacement de l'objet
  -- @param pColor (OPTIONNEL) couleur du sprite par défaut (utile si aucune image n'est définie pour l'afficher)
  function self.initialize (pX, pY, pWidth, pHeight, pImage, pVelocityJump, pStartSpeed, pFriction, pStartGravity, pVelocityMax, pWeight, pLiveImagePath, pLevel, pLivesStart, pScore, pMoveWithVelocity, pMoveWithMouse, pMoveWithKeys,pColor)
    debugFunctionEnter("player.initialize ", pX, pY, pWidth, pHeight, pImage, pVelocityJump, pStartSpeed, pFriction, pStartGravity, pVelocityMax, pWeight, pLiveImagePath, pLevel, pLivesStart, pScore, pMoveWithVelocity, pMoveWithMouse, pMoveWithKeys,pColor)
    assertEqualQuit(viewport, nil, "player.initialize:viewport", true)
    assertEqualQuit(settings.playerKeys, nil, "player.initialize:settings.playerKeys", true)
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (map.get_type() == MAP_TYPE_NOT_TILED) then
      if (pWidth == nil) then pWidth = 32 end
      if (pHeight == nil) then pHeight = 32 end
    else
      if (pWidth == nil) then pWidth = map.get_tileWidth() end
      if (pHeight == nil) then pHeight = map.get_tileHeight() end
    end
    if (pVelocityJump == nil) then pVelocityJump = 0 end
    if (pStartSpeed == nil) then pStartSpeed = 0 end
    if (pFriction == nil) then pFriction = settings.friction end
    if (pStartGravity == nil) then pStartGravity = settings.gravity end
    if (pVelocityMax == nil) then pVelocityMax = 0 end

    if (pWeight == nil) then pWeight = 1 end
    if (pLiveImagePath == nil) then pLiveImagePath = MAP_PLAYER_FOLDER.."/player_life.png" end
    if (pLivesStart == nil) then pLivesStart = 3 end
    if (pLevel == nil) then pLevel = 1 end
    if (pScore == nil) then pScore = 0 end
    if (pMoveWithVelocity == nil) then pMoveWithVelocity = true end
    if (pMoveWithKeys == nil) then pMoveWithKeys = true end
    if (pMoveWithMouse == nil) then pMoveWithMouse = true end

    super.set_type(SPRITE_TYPE_PLAYER)
    super.set_canJump(true)

    -- attributs de la classe parent
    -------------
    super.set_x(pX)
    super.set_y(pY)
    super.set_width(pWidth)
    super.set_height(pHeight)
    -- Note: les valeurs suivantes sont divisées par l'échelle de l'écran
    super.set_velocityJump(math.floor(pVelocityJump / SCREEN_SCALE_Y))
    super.set_startSpeed(math.floor(pStartSpeed / SCREEN_SCALE_Y))
    super.set_speed(math.floor(pStartSpeed / SCREEN_SCALE_X))
    super.set_friction(math.floor(pFriction / SCREEN_SCALE_X))
    super.set_startGravity(math.floor(pStartGravity / SCREEN_SCALE_Y))
    super.set_gravity(math.floor(pStartGravity / SCREEN_SCALE_Y))
    super.set_velocityMax(math.floor(pVelocityMax / SCREEN_SCALE_X))

    super.set_weight(pWeight)
    super.set_gravityModularity(0)
    super.set_moveWithVelocity(pMoveWithVelocity) --NOTE. si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
    super.set_color(pColor)

    -- attributs spécifiques à cette classe
    -------------
    moveWithMouse  = pMoveWithMouse
    moveWithKeys   = pMoveWithKeys
    keys           = settings.playerKeys -- par défaut on utilise les touches de clavier définies dans les réglages du jeu
    level          = pLevel
    livesImagePath = pLiveImagePath
    score          = pScore
    livesStart     = pLivesStart
    livesLeft      = livesStart
    hasWon         = false

    --Touches devant être relachées avant une action.
    --keysToRelease[settings.playerKeys.moveDown] = false
    --keysToRelease[settings.playerKeys.moveDownAlt] = false

    -- définit les hotSpots du joueur pour les collisions
    super.createCollisionBox(COLLISION_MODEL_BOX6)
    
    debugVar = super.tostring() -- Cette variable globale permet de voir les infos du sprite dans la pile d'execution en debug 
  end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_moveWithKeys ()
    return moveWithKeys
  end
  function self.set_moveWithKeys (pValue)
    moveWithKeys = pValue
  end
  function self.get_moveWithMouse ()
    return moveWithMouse
  end
  function self.set_moveWithMouse (pValue)
    moveWithMouse = pValue
  end
  function self.get_keys ()
    return keys
  end
  function self.set_keys (pValue)
    keys = pValue
  end
  function self.get_level ()
    return level
  end
  function self.set_level (pValue)
    level = pValue
  end
  function self.get_livesImagePath ()
    return livesImagePath
  end
  function self.set_livesImagePath (pValue)
    livesImagePath = pValue
  end
  function self.get_score ()
    return score
  end
  function self.set_score (pValue)
    score = pValue
  end
  function self.get_livesLeft ()
    return livesLeft
  end
  function self.set_livesLeft (pValue)
    livesLeft = pValue
  end
  function self.get_livesStart ()
    return livesStart
  end
  function self.set_livesStart (pValue)
    livesStart = pValue
  end
  function self.get_hasWon ()
    return hasWon
  end
  function self.set_hasWon (pValue)
    hasWon = pValue
  end
  function self.get_keys ()
    return keys
  end
  function self.set_keys (pValue)
    keys = pValue
  end
  function self.get_keysToRelease ()
    return keysToRelease
  end
  function self.set_keysToRelease (pValue)
    keysToRelease = pValue
  end
  function self.get_keyToRelease (pIndex)
    return keysToRelease[pIndex]
  end
  function self.set_keyToRelease (pIndex, pValue)
    keysToRelease[pIndex] = pValue
  end

  -- Override des fonctions des parents
  --------------------------------
  --- Détruit l'objet (Override)
  function self.destroy ()
    debugFunctionEnter("player.destroy")
    self.clean()
    self = nil
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu (Override)
  function self.clean ()
    debugFunctionEnter("player.clean")
    local backgroundColor

    if (assertEqual(viewport.backgroundColor, nil, "player.destroy:backgroundColor")) then
      backgroundColor = {0, 0, 0}
    else
      backgroundColor = viewport.backgroundColor
    end

    -- efface l'objet
    love.graphics.setColor(backgroundColor[1], backgroundColor[2], backgroundColor[3])
    HUD.rectangleScale("fill", super.get_x(), super.get_y(), super.get_width(), super.get_height())
    love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
  end

  --- Actualise l'objet (Override)
  -- @param pDt delta time
  function self.update (pDt)
    --debugFunctionEnter("player.update ", pDt) --ATTENTION  cet appel peut remplir le log
    -- attention les spécificités des sprites de type player SONT et DOIVENT être traités dans la fonction sprite.draw() (donc par super.draw())
    super.update(pDt)
  end

  --- Dessine l'objet (Override)
  function self.draw ()
    --debugFunctionEnter("player.draw") --ATTENTION cet appel peut remplir le log
    -- attention les spécificités des sprites de type player SONT et DOIVENT être traités dans la fonction sprite.draw() (donc par super.draw())
    super.draw()
  end

  --- Déplace l'objet (Override)
  -- @param pDt delta time
  function self.move (pDt)
    --debugFunctionEnter("player.move:", pDt) --ATTENTION cet appel peut remplir le log
    -- attention les spécificités des sprites de type player SONT et DOIVENT être traités dans la fonction sprite.move() (donc par super.move())
    super.move(pDt)
  end

  --- Vérifie les collisions et adapte le mouvement en conséquence (Override)
  -- En cas de collision, rectifie les attributs x et y de l'objet
  -- @param pCollisionXmin (OPTIONNEL) dimension X minimale à vérifier
  -- @param pCollisionXmax (OPTIONNEL) dimension X maximale à vérifier
  -- @param pCollisionYmin (OPTIONNEL) dimension Y minimale à vérifier
  -- @param pCollisionYmax (OPTIONNEL) dimension Y maximale à vérifier
  -- @return true si il y a une collision ou false sinon
  function self.collide (pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax)
    --debugFunctionEnter("player.collide ", pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "player.collide:viewport", true)
    -- attention les spécificités des sprites de type player SONT et DOIVENT être traités dans la fonction sprite.collide() (donc par super.collide())
    return super.collide(pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax - 50 )
  end

  --- place l'objet dans la zone de jeu (Override)
  function self.spawn ()
    debugFunctionEnter("player.spawn")
    assertEqualQuit(viewport, nil, "player.spawn:viewport", true)
    super.set_x((viewport.get_width() - super.get_width()) / 2)
    super.set_y((viewport.get_height() - super.get_height() - viewport.charHeight * 2) / 2)
  end

  -- fonctions spécifiques
  --------------------------------
  --- Interactions de l'objet avec le clavier
  -- @param pScancode (OPTIONNEL) le scancode représentant la touche pressée
  -- @param pIsrepeat (OPTIONNEL) TRUE si cet événement keypress est une répétition. Le délai entre les répétitions des touches dépend des paramètres système de l'utilisateur
  function self.keypressed (pKey, pScancode, pIsrepeat)
    --debugFunctionEnter("player.keyPressed ", pKey, pScancode, pIsrepeat)

    -- IMPORTANT
    -- ON NE TRAITE ICI QUE LES TOUCHES QUI NE SONT PAS EN RAPPORT AVEC LES MOUVEMENT DU JOUEUR
    -- CES DERNIÈRES SONT ET DOIVENT ÊTRE TRAITÉES DANS LA FONCTION SPRITE.UPDATEPLAYER())

    -- Activer les déplacements avec le clavier (ON/OFF).
    if (DEBUG_MODE and pKey == settings.appKeys.moveWithKeys) then
      local state = not moveWithKeys
      debugMessage("player.moveWithKeys=", state)
      moveWithKeys = state
    end
    -- Activer les déplacements avec la souris (ON/OFF).
    if (DEBUG_MODE and pKey == settings.appKeys.moveWithMouse) then
      local state = not moveWithMouse
      debugMessage("player.moveWithMouse=", state)
      moveWithMouse = state
    end
  end

  --- Interactions de l'objet avec la souris
  -- @param pX (OPTIONNEL) la position de la souris sur l'axe des x.
  -- @param pY (OPTIONNEL) la position de la souris sur l'axe des y.
  -- @param pDX (OPTIONNEL) La quantité a été déplacée le long de l'axe des abscisses depuis la dernière fois que la fonction a été appelée.
  -- @param pDY (OPTIONNEL) La quantité a évolué le long de l'axe des y depuis la dernière fois que la fonction a été appelée.
  -- @param pIstouch (OPTIONNEL) Vrai si le bouton de la souris appuie sur la touche à partir d'une touche tactile à l'écran tactile.
  function self.mousemoved (pX, pY, pDX, pDY, pIstouch)
    --debugFunctionEnter("player.mousemoved ", pX, pY, pDX, pDY, pIstouch)  --ATTENTION cet appel peut remplir le log
    if (appState.get_currentState() == SCREEN_PLAY) then
      if (moveWithMouse) then
        -- déplacements avec la souris
        --super.set_x (pX - super.get_width() / 2)
        --super.set_y (pY - super.get_height() / 2)
        super.set_vX(super.get_vX() + super.get_speed() * settings.mouse.sensibilityX * pDX)
        super.set_vY(super.get_vY() + super.get_speed() * settings.mouse.sensibilityY * pDY)
        --debugMessage("pDX,pDY=", pDX ,pDY, "VX,VY=", super.get_vX() ,super.get_vY())
      end
    end --if (appState.get_currentState() == SCREEN_PLAY) then
  end

  --- Intercepte le clic de souris
  -- @param pX position x de la souris, en pixels.
  -- @param pY position y de la souris, en pixels.
  -- @param pButton L'index du bouton qui a été pressé: 1 est le bouton principal, 2 est le bouton secondaire et 3 est le bouton du milieu.
  -- @param pIstouch (OPTIONNEL) Vrai si le bouton est une touche tactile.
  function self.mousepressed (pX, pY, pButton, pIstouch)
    --debugFunctionEnter("player.mousepressed ", pX, pY, pButton, pIstouch)
  end

  --- Décrémente la vie du joueur
  -- @param pValue (OPTIONNEL) valeur à décrémenter
  -- @return le nombre de vies mis à jour
  function self.loseLife (pValue)
    livesLeft = livesLeft - pValue
    return livesLeft
  end

  --- Incrémente le score du joueur
  -- @param pValue (OPTIONNEL) valeur à ajouter
  -- @return le score mis à jour
  function self.addScore (pValue)
    if (pValue == nil) then pValue = 1 end
    score = score + pValue
    return score
  end

  --- Incrémente le niveau du joueur
  -- @param pValue (OPTIONNEL) valeur à ajouter
  -- @return le niveau mis à jour
  function self.addLevel (pValue)
    if (pValue == nil) then pValue = 1 end
    level = level + pValue
    return level
  end

  -- initialisation par défaut
  self.initialize()
  return self
end