-- Entité représentant un sprite
-- Utilise une forme d'héritage simple: sprite qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent.
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/functions")

-- type de collision pour les hotspots
COLLIDE_SOLID = "collide_solid"
COLLIDE_JUMPTHROUGH = "collide_jumpthrough"

-- type de hotspots
HOTSPOT_TOP = "hotspot_top"
HOTSPOT_RIGHT = "hotspot_right"
HOTSPOT_BOTTOM = "hotspot_bottom"
HOTSPOT_LEFT = "hotspot_left"
HOTSPOT_UNIQUE = "hotspot_unique"

-- modèles de collision (ie. combinaisons de hotspots)
COLLISION_MODEL_BOX4 = "collision_model_box4"
COLLISION_MODEL_BOX6 = "collision_model_box6"
COLLISION_MODEL_POINT = "collision_model_point"
COLLISION_MODEL_NONE = "collision_model_none"

-- type de sprite
-- NOTE: le __ est important dans les libellés et les valeurs car il permet de différencier le type de sprite et le sous-type
SPRITE_TYPE_UNDEFINED = "sprite_type_undefined"
SPRITE_TYPE_PLAYER = "sprite_type_player"
-- un objet
SPRITE_TYPE_PROP = "sprite_type_prop"
-- un élément de décor  
SPRITE_TYPE_PROP__COIN = SPRITE_TYPE_PROP.."__coin"
SPRITE_TYPE_ELT = "sprite_type_elt"
SPRITE_TYPE_ELT__DOOR = SPRITE_TYPE_ELT.."__door"
-- un PNJ
SPRITE_TYPE_PNJ = "sprite_type_pnj" 
SPRITE_TYPE_PNJ__BLOB = SPRITE_TYPE_PNJ.."__blob" 

-- états possibles
SPRITE_STATE_NORMAL = "sprite_state_normal"
SPRITE_STATE_REMOVING = "sprite_state_removing"
SPRITE_STATE_REMOVED = "sprite_state_removed"

-- type d'animation
SPRITE_ANIM_NONE = "sprite_anim_none"
SPRITE_ANIM_IDLE = "sprite_anim_idle"
SPRITE_ANIM_WALK = "sprite_anim_walk"
SPRITE_ANIM_RUN = "sprite_anim_run"
SPRITE_ANIM_CLIMB = "sprite_anim_climb"
SPRITE_ANIM_CLIMB_IDLE = "sprite_anim_climb_idle"
SPRITE_ANIM_OPEN = "sprite_anim_open"
SPRITE_ANIM_CLOSE = "sprite_anim_close"
SPRITE_ANIM_DESTROY = "sprite_anim_destroy"
SPRITE_ANIM_DIE = "sprite_anim_die"

-- direction de déplacement
SPRITE_MOVE_LEFT = "sprite_move_left"
SPRITE_MOVE_RIGHT = "sprite_move_right"
SPRITE_MOVE_TOP = "sprite_move_top"
SPRITE_MOVE_BOTTOM = "sprite_move_bottom"

-- liste des sprites utilisés dans le jeu
spriteList = {}

--- Crée un pseudo objet de type sprite
-- le sprite est automatiquement ajouté à la variable spriteList
-- @return le pseudo objet sprite créé et l'index du sprite dans la liste
function newSprite ()
  -- création d'un objet parent
  --local super = newObject()
  -- création d'un objet enfant
  local self = class:new()

  -- variables locales 
  -------------
  -- timer pour gérer la granularité de la gravité
  local timerDrop = 0
  -- flag pour déterminer si le sprite a été udpaté au moins une fois (utilisé par la CCD)
  local firstUpdateDone = false
  -- flag pour déterminer si le sprite a eu une collision de type CCD  
  local isCCDcollision = false
  
  -- attributs privés accessibles via les getter/setters
  -------------
    -- position en X dans le viewport
  local x = 0
  -- position en Y dans le viewport
  local y = 0
  -- largeur
  local width = 0
  -- hauteur
  local height = 0
  -- dernière position actualisées (utilisée par le CCD)
  local lastPosition = {}
  -- vitesse de départ
  local startSpeed = 0
  -- vitesse courante (modifiée par l'environnement ou les actions du joueur)
  local speed = 0
  -- rotation en degrés (pour l'application de la vitesse)
  local rotation = 0
  -- vitesse en X
  local vX = 0
  -- vitesse en Y
  local vY = 0
  -- vélocité maximale
  -- NOTE: si la valeur est nulle, l'objet ne pourra pas se déplacer
  local velocityMax = 0
  -- vélocité pour un saut
  local velocityJump = 0
  -- poids (influence la gravité et la résistance de l'environnement)
  -- NOTE: si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage
  local weight = 0
  -- index du sprite dans la table (utile pour sa suppression)
  local tableIndex = 0
  -- différencier le personnage principal, les ennemis, les objets…
  local type = SPRITE_TYPE_UNDEFINED
  -- freinage du à l'environnement'
  local friction = 0
  -- gravité initiale
  local startGravity = 0
  -- gravité courante (modifiée par l'environnement ou les actions du joueur)
  local gravity  = 0
  -- le sprite est posé sur le décor ou chute t-il ?
  local isStanding = false
  -- le sprite peut il sauter ?
  local canJump = false
  -- le sprite est en train de sauter ?
  local isJumping = false
  -- liste des points de vérification des collisions (hotspot)
  local hotSpots = {}
  -- liste des images
  local images  = {}
  -- liste des animations
  local animations  = {}
  -- nom de l'animation en cours
  local currentAnimation = SPRITE_ANIM_NONE
  -- index de la frame en cours (commence à 1)
  local frame = 0
  -- la durée d'une frame en secondes
  local animationSpeed = 0 
  -- timer pour la durée restante de la frame courante
  local animationTimer = 0 
  -- l'animation courante doit elle être inversée ?
  local flip = false
  -- état du sprite (est il en cours de suppression ?)
  local state = SPRITE_STATE_NORMAL
  -- utilise les vélocités pour le déplacement de l'objet (mouvement automatique)
  -- NOTE: si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
  local moveWithVelocity = true
  -- modularité de la gravité
  -- NOTE: si la valeur est nulle, la gravité sera appliquée en continu (déplacement fluide), sinon elle sera appliquée avec la granularité définie (en pixel)
  local gravityModularity = 0
  -- couleur d'affichage par défaut
  local color  = {}
  -- direction du déplacement (PNJ)
  local direction = SPRITE_MOVE_LEFT
  
  --- Initialise l'objet
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pType type de sprite (voir les constantes SPRITE_TYPE_xxx)
  -- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur 0 sera utilisée.
  -- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur 0 sera utilisée.
  -- @param pWidth (OPTIONNEL) largeur en pixel. Si absent la valeur map.get_tileWidth() sera utilisée.
  -- @param pHeight (OPTIONNEL) hauteur en pixel. Si absent la valeur map.get_tileHeight() sera utilisée.
  -- @param pImage (OPTIONNEL) image principale du sprite.
  -- @param pVelocityJump (OPTIONNEL) vélocité pour un saut. Si absent la valeur 0 sera utilisée.
  -- @param pStartSpeed (OPTIONNEL) vitesse initiale. Si absent la valeur 0 sera utilisée.
  -- @param pFriction (OPTIONNEL) freinage du à l'environnement. Si absent la valeur 0 sera utilisée.
  -- @param pStartGravity (OPTIONNEL) chute due à la gravité. Si absent la valeur 0 sera utilisée.
  -- @param pVelocityMax (OPTIONNEL) vélocité maximale. Si absent la valeur 0 sera utilisée.
  -- @param pMoveWithVelocity (OPTIONNEL) true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique)
  -- @param pColor (OPTIONNEL) couleur du sprite par défaut (utile si aucune image n'est définie pour l'afficher)
  -- @param pGravityModularity (OPTIONNEL) modularité de la gravité
  function self.initialize (pType, pX, pY, pWidth, pHeight, pImage, pVelocityJump, pStartSpeed, pFriction, pStartGravity, pVelocityMax, pMoveWithVelocity, pColor, pGravityModularity)
    debugFunctionEnter("sprite.initialize ", pType, pX, pY, pWidth, pHeight, pImage, pVelocityJump, pSpeed, pFriction, pStartGravity, pVelocityMax, pMoveWithVelocity, pColor, pGravityModularity)
    assertEqualQuit(viewport, nil, "sprite.initialize:viewport", true)
    if (pType == nil) then pType = SPRITE_TYPE_UNDEFINED end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (map.get_type() == MAP_TYPE_NOT_TILED) then
      if (pWidth == nil) then pWidth = 32 end
      if (pHeight == nil) then pHeight = 32 end
    else
      if (pWidth == nil) then pWidth = map.get_tileWidth() end
      if (pHeight == nil) then pHeight = map.get_tileHeight() end
    end
    if (pVelocityJump == nil) then pVelocityJump = 0 end
    if (pStartSpeed == nil) then pStartSpeed = 0 end
    if (pFriction == nil) then pFriction = settings.friction end
    if (pStartGravity == nil) then pStartGravity = settings.gravity end
    if (pVelocityMax == nil) then pVelocityMax = 0 end
    if (pMoveWithVelocity == nil) then pMoveWithVelocity = true end
    if (pGravityModularity == nil) then pGravityModularity = 0 end
    if (pColor == nil) then pColor = {128, 128, 128, 255} end
    
    -- attributs spécifiques à cette classe
    -------------
    x = pX
    y = pY
    width = pWidth
    height = pHeight

    -- Note: certaines valeurs sont divisées par l'échelle de l'écran
    startSpeed        = math.floor(pStartSpeed / SCREEN_SCALE_X)
    speed             = startSpeed
    rotation          = 90
    vX         = 0
    vY         = 0
    velocityMax       = math.floor(pVelocityMax / SCREEN_SCALE_X)
    velocityJump      = math.floor(pVelocityJump / SCREEN_SCALE_Y)
    weight            = 0 -- pas de chute par défaut
    --tableIndex        = 0 -- mis à jour lors de l'ajout spriteList, ne pas remettre à 0 dans l'initialize
    type              = pType
    friction          = math.floor(pFriction / SCREEN_SCALE_X)
    gravity           = math.floor(pStartGravity / SCREEN_SCALE_Y)
    StartGravity      = math.floor(pStartGravity / SCREEN_SCALE_Y)
    isStanding        = false
    canJump           = false
    isJumping         = false
    hotSpots          = {}
    images            = {}
    animations        = {}
    currentAnimation  = SPRITE_ANIM_NONE
    frame             = 0
    animationSpeed    = 1 / 8
    animationTimer    = 1 / 8
    flip              = false
    state             = SPRITE_STATE_NORMAL
    moveWithVelocity  = pMoveWithVelocity
    gravityModularity = pGravityModularity
    timerDrop         = 0
    color             = pColor
    direction         = SPRITE_MOVE_RIGHT
    
    if (pImage ~= nil and pImage ~= "" and love.filesystem.isFile(pImage)) then images[1] = love.graphics.newImage(pImage) end
    
    if (type == SPRITE_TYPE_PNJ) then self.createCollisionBox(COLLISION_MODEL_BOX4) else  self.createCollisionBox(COLLISION_MODEL_POINT) end
    
    debugVar = self.tostring() -- Cette variable globale permet de voir les infos du sprite dans la pile d'execution en debug 

  end

--- Permet de récupérer le contenu de la table en utilisant print() ou tostring().
function self.tostring ()
  -- NOTE: il est impossible de parcourir les attribut de cet objet car ce sont de variable locales et non des élément d'une table
  return "type="..type
    .." tableIndex="..tableIndex
    .." x="..x
    .." y="..y
    .." vX="..vX
    .." vY="..vY
    .." width="..width
    .." height="..height
    .." velocityJump="..velocityJump
    .." velocityMax="..velocityMax
    .." speed="..speed
    .." friction="..friction
    .." weight="..weight
    .." gravity="..gravity
    .." gravityModularity="..gravityModularity
    .." state="..state
    .." canJump="..tostring(canJump)
    .." isStanding="..tostring(isStanding)
    .." flip="..tostring(flip)
    .." frame="..frame
    .." currentAnimation="..currentAnimation
    .." direction="..direction
end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_x ()
    return x
  end
  function self.set_x (pValue)
    x = pValue
    lastPosition.x = x
  end
  function self.get_y ()
    return y
  end
  function self.set_y (pValue)
    y = pValue
    lastPosition.y = y
  end
  function self.get_width ()
    return width
  end
  function self.set_width (pValue)
    width = pValue
  end
  function self.get_height ()
    return height
  end
  function self.set_height (pValue)
    height = pValue
  end
  function self.get_startSpeed ()
    return startSpeed
  end
  function self.set_startSpeed (pValue)
    startSpeed = pValue
  end
  function self.get_speed ()
    return speed
  end
  function self.set_speed (pValue)
    speed = pValue
  end
  function self.get_rotation ()
    return rotation
  end
  function self.set_rotation (pValue)
    rotation = pValue
  end
  function self.get_vX ()
    return vX
  end
  function self.set_vX (pValue)
    vX = pValue
  end
  function self.get_vY ()
    return vY
  end
  function self.set_vY (pValue)
    vY = pValue
  end
  function self.get_velocityMax ()
    return velocityMax
  end
  function self.set_weight (pValue)
    weight = pValue
  end
  function self.get_weight ()
    return weight
  end
  function self.set_velocityMax (pValue)
    velocityMax = pValue
  end
  function self.get_velocityJump ()
    return velocityJump
  end
  function self.set_velocityJump (pValue)
    velocityJump = pValue
  end
  function self.get_tableIndex ()
    return tableIndex
  end
  function self.set_tableIndex (pValue)
    tableIndex = pValue
  end
  function self.get_type ()
    return type
  end
  function self.set_type (pValue)
    type = pValue
  end
  function self.get_friction()
    return friction
  end
  function self.set_friction (pValue)
    friction = pValue
  end
  function self.get_gravity()
    return gravity
  end
  function self.set_gravity (pValue)
    gravity = pValue
  end
  function self.get_startGravity ()
    return startGravity
  end
  function self.set_startGravity (pValue)
    startGravity = pValue
  end
  function self.get_isStanding()
    return isStanding
  end
  function self.set_isStanding (pValue)
    isStanding = pValue
  end
  function self.get_canJump()
    return canJump
  end
  function self.set_canJump (pValue)
    canJump = pValue
  end
  function self.get_isJumping ()
    return isJumping
  end
  function self.set_isJumping (pValue)
    isJumping = pValue
  end
  function self.get_hotSpots()
    return hotSpots
  end
  function self.set_hotSpots (pValue)
    hotSpots = pValue
  end
  function self.get_images()
    return images
  end
  function self.set_images (pValue)
    images = pValue
  end
  function self.get_animations()
    return animations
  end
  function self.set_animations (pValue)
    animations = pValue
  end
  function self.get_currentAnimation()
    return currentAnimation
  end
  function self.set_currentAnimation (pValue)
    currentAnimation = pValue
  end
  function self.get_frame()
    return frame
  end
  function self.set_frame (pValue)
    frame = pValue
  end
  function self.get_animationSpeed()
    return animationSpeed
  end
  function self.set_animationSpeed (pValue)
    animationSpeed = pValue
  end
  function self.get_animationTimer()
    return animationTimer
  end
  function self.set_animationTimer (pValue)
    animationTimer = pValue
  end
  function self.get_flip()
    return flip
  end
  function self.set_flip (pValue)
    flip = pValue
  end
  function self.get_state()
    return state
  end
  function self.set_state (pValue)
    state = pValue
  end
  function self.get_moveWithVelocity ()
    return moveWithVelocity
  end
  function self.set_moveWithVelocity (pValue)
    moveWithVelocity = pValue
  end
  function self.get_gravityModularity ()
    return gravityModularity
  end
  function self.set_gravityModularity (pValue)
    gravityModularity = pValue
  end
  function self.get_color ()
    return color
  end
  function self.set_color (pValue)
    color = pValue
  end
  function self.get_direction ()
    return direction
  end
  function self.set_direction (pValue)
    direction = pValue
  end
  --- retourne l'identifiant (simple) du sprite
  --@return string
  function self.get_Id()
    return type.."-"..tableIndex
  end
  --- retourne la dernière position mis à jour
  --@return table {x,y}
  function self.get_lastPosition ()
    return lastPosition
  end
  --- actualise la dernière position en fonction des positions x et y
  function self.updateLastPosition ()
     lastPosition.x = x
     lastPosition.y = y
  end  
  
  --- détruit le sprite en jouant éventuellement une animation SPRITE_ANIM_DESTROY lors de sa destruction (Override)
  function self.destroy ()
    debugFunctionEnter("sprite.destroy ")
    -- si une animation pour la destruction du sprite existe, on la joue
    if (animations[SPRITE_ANIM_DESTROY] ~= nil) then
      self.playAnimation(SPRITE_ANIM_DESTROY)
      state = SPRITE_STATE_REMOVING
    end
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu (Override)
  --- supprime définitivement le sprite de la liste des sprites
  function self.clean ()
    --debugFunctionEnter("sprite.clean")
    debugMessage("suppression ",self.get_Id(), tableIndex)
    tableIndex = 0 -- WTF ? en mettant l'index de l'élément à supprimer à 0 on supprime le bon élément, en mettant la valeur de table index, cela supprime le mauvais 
    if (spriteList ~= nil) then table.remove(spriteList, tableIndex) end
    state = SPRITE_STATE_REMOVED
  end

  --- Dessine l'objet (Override)
  function self.draw ()
    --debugFunctionEnter("sprite.draw") --ATTENTION cet appel peut remplir le log
    if (state == SPRITE_STATE_REMOVED) then return end

    -- le sprite possède t-il une animation en cours de lecture ?
    if (currentAnimation ~= SPRITE_ANIM_NONE and animations[currentAnimation] ~= nil) then
      --Oui, on récupère l'image de la frame en cours et on l'affiche
      local imgName = animations[currentAnimation][frame]
      local img = images[imgName]
      local halfw = img:getWidth()  / 2
      local halfh = img:getHeight() / 2
      local flipCoef = 1
      if flip then flipCoef = -1 end

      love.graphics.draw(img, x + halfw, y + halfh, 0, flipCoef, 1, halfw, halfh)
    else
      --non, le sprite a t'il au moins une image ?
      if (images ~= nil and #images > 0) then
        -- oui, par défaut on affiche la première image du sprite
        HUD.drawImageScale(images[1], x, y, 0, 1, 1, 0, 0)
      else
        --non, on affiche un simple carré
        love.graphics.setColor(color[1], color[2], color[3], color[4])
        HUD.rectangleScale("fill", x, y, width, height)
        love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
      end
    end
    if (DEBUG_MODE >= 3) then 
      -- affiche l'id du sprite 
      local drawX = x 
      local drawY = y 
      local content = tableIndex
      love.graphics.setColor(255, 0, 0, 255)
      love.graphics.setFont(HUD.get_fontDebug())
      love.graphics.print(content, drawX, drawY)
      love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
    end
  end

  --- verifie les collisions des sprites de type PNJ
  --@return true en cas de collision, false sinon
  function self.collidePNJ ()
    --debugFunctionEnter("sprite.collidePNJ)") --ATTENTION cet appel peut remplir le log
    local isCollision = false
    -- Tuile sous le sprite
    local idOverlap = map.getTileAt(x + map.get_tileWidth() / 2, y + map.get_tileHeight() - 1)
    local tileType = nil
    
    if (map.get_tiles()[idOverlap] ~= nil)  then tileType = map.get_tiles()[idOverlap].type end
    
    if (type == SPRITE_TYPE_PNJ__BLOB) then
      -- mouvement pour les sprite de type SPRITE_TYPE_PNJ__BLOB
      if (tileType == TILE_GO_RIGHT or (direction == SPRITE_MOVE_LEFT and self.collideLeft())) then
        -- tuiles pour aller vers la droite ou collision avec une tuile solide
        direction = SPRITE_MOVE_RIGHT
        flip = false
        isCollision = true
        x = x + 2
      elseif (tileType == TILE_GO_LEFT or (direction == SPRITE_MOVE_RIGHT and self.collideRight())) then
        -- tuiles pour aller vers la gauche ou collision avec une tuile solide
        direction = SPRITE_MOVE_LEFT
        flip = true
        isCollision = true
        x = x - 2
      end
    end
    return isCollision
  end
  
  --- place l'objet dans la zone de jeu (Override)
  function self.spawn ()
    debugFunctionEnter("sprite.spawn")
    assertEqualQuit(viewport, nil, "sprite.spawn:viewport", true)
    x = (viewport.get_width() - width) / 2
    y = (viewport.get_height() - height - viewport.charHeight * 2) / 2
    timerDrop = 0 
  end

  -- fonctions spécifiques
  --------------------------------
  --- place l'objet dans la carte de jeu
  function self.spawnToMap (pCol, pLine)
    debugFunctionEnter("sprite.spawnToMap ", pCol, pLine)
    if (map ~= nil and map.get_playerStart() ~= nil) then
      if (pCol == nil) then pCol = map.get_playerStart().col end
      if (pLine == nil) then pLine = map.get_playerStart().line end
    else
      if (pCol == nil) then pCol = 1 end
      if (pLine == nil) then pLine = 1 end
    end
    local posX, posY = map.mapToPixel (pCol, pLine)
    x = posX
    y = posY
  end

   --- Aligne le sprite sur la ligne ou il se trouve
  function self.alignOnLine ()
    --debugFunctionEnter("sprite.alignOnLine")
    y = math.floor((y + map.get_tileHeight() / 2) / map.get_tileHeight()) * (map.get_tileHeight())
  end

  --- Aligne le sprite sur la colonne o il se trouve
  function self.alignOnColumn ()
    --debugFunctionEnter("sprite.alignOnColumn")
    x = math.floor((x + map.get_tileWidth() / 2) / map.get_tileWidth()) * (map.get_tileWidth())
  end

  --- ajoute un hotSpot (point de vérification des collision) au sprite
  -- NOTe : la fonction createCollisionBox() permet de crééer des ensembles de hotspot à partir de modèles prédéfinis
  -- @param pOffsetX décalage en X du point par rapport à l'origine du sprite
  -- @param pOffsetY décalage en Y du point par rapport à l'origine du sprite
  -- @param pType type de hotspot permettant de savoir quelles collisions tester (voir les constantes HOTSPOT_xxx)
  -- @param OPTIONNEL pKey clé du hotspot dans la liste, elle doit être unique. Si absent, la clé utilisée sera égale au le nombre d'éléments existant dans la liste (index numérique croissant)
  function self.addHotSpot (pOffsetX, pOffsetY, pType, pKey)
    --debugFunctionEnter("sprite.addHotSpot )", pOffsetX, pOffsetY, pType, pKey)
    if (pKey == nil) then pKey = #hotSpots end
    hotSpots[pKey] = {x = pOffsetX, y = pOffsetY, type = pType}
  end

  --- Vérifie si un hotspot d'un type donnée entre en collision
  -- @param pType: type de hotspot (voir les constantes HOTSPOT_xxx)
  -- @return true ou false
  function self.isHotSpotCollide (pType)
    --debugFunctionEnter("sprite.isHotSpotCollide )", pType) --ATTENTION cet appel peut remplir le log
    local key, hotspot
    -- on boucle sur tous les hotSpot définis pour le joueur
    for key, hotspot in pairs(hotSpots) do
      -- index de la tuile située sous le hotSpot
      indexTile = map.getTileAt(x + hotspot.x, y + hotspot.y)
      if ((hotspot.type == pType or hotspot.type == HOTSPOT_UNIQUE)-- hot spot du type attendu ou de type HOTSPOT_UNIQUE
        and indexTile ~= nil and indexTile ~= TILE_SPECIAL_EMPTY -- tuile existante
      ) then
          if (map.isSolid(indexTile)) then 
            return COLLIDE_SOLID -- tuile solide
          elseif (map.isJumpThrough(indexTile)) then 
            return COLLIDE_JUMPTHROUGH -- tuile jumpThrough
          end
        end
    end

    return false
  end

  --- Vérifie si le sprite entre en collision à droite
  -- @return true ou false
  function self.collideRight ()
    --debugFunctionEnter("sprite.collideRight )") --ATTENTION cet appel peut remplir le log
    return (self.isHotSpotCollide(HOTSPOT_RIGHT) == COLLIDE_SOLID)
  end

  --- Vérifie si le sprite entre en collision à gauche
  -- @return true ou false
  function self.collideLeft ()
    --debugFunctionEnter("sprite.collideLeft )") --ATTENTION cet appel peut remplir le log
    return (self.isHotSpotCollide(HOTSPOT_LEFT) == COLLIDE_SOLID)
  end

  --- Vérifie si le sprite entre en collision en haut
  -- @return true ou false
  function self.collideTop ()
    --debugFunctionEnter("sprite.collideTop )") --ATTENTION cet appel peut remplir le log
    return (self.isHotSpotCollide(HOTSPOT_TOP) == COLLIDE_SOLID)
  end

  --- Vérifie si le sprite entre en collision en bas
  -- @return true ou false
  function self.collideBottom ()
    --debugFunctionEnter("sprite.collideBottom )") --ATTENTION cet appel peut remplir le log
    local test = self.isHotSpotCollide(HOTSPOT_BOTTOM)
    if (test == COLLIDE_SOLID) then
      -- tuile solide
      return true
    elseif (test == COLLIDE_JUMPTHROUGH) then
      -- tuile jumpThrough
      local lineY = math.floor((y + map.get_tileHeight() / 2) / map.get_tileHeight()) * map.get_tileHeight()
      local distance = y - lineY
      if (distance >= 0 and distance < 10) then return true end
    end
    return false
  end

  --- Vérifie si le sprite entre en collision avec un autre sprite (BoxCollision)
  -- @param pSprite autre sprite à vérifier
  -- @return true ou false
  function self.boxCollision (pSprite)
    --debugFunctionEnter("sprite.boxCollision )", pSprite) --ATTENTION cet appel peut remplir le log
    if (pSprite == nil) then return false end
    return boxCollision(x, y, width, height, pSprite.get_x(), pSprite.get_y(), pSprite.get_width(), pSprite.get_height()) --NOTE: on garde les get car on peut avoir en paramètre un sprite ou un player
  end

  --- Charge des images depuis unes liste de nom et un dossier
  -- Les images sont ajoutées à l'attribut images.
  -- @param pFolder le chemin du sous répertoire contenant les images de l'animation
  -- @param pImageList un tableau de chaînes de caractère contenant le nom des images
  -- @param OPTIONNEL pExt extension de fichier à ajouter au nom des images. Si absent,".png" sera utilisé
  function self.addImages (pFolder, pImageList, pExt)
    --debugFunctionEnter("sprite.addImages )", pFolder, pImageList, pExt)
    if (pExt == nil) then pExt  = ".png" end
    for k, v in pairs(pImageList) do
      local fileName = pFolder.."/"..v..pExt
      images[v] = love.graphics.newImage(fileName)
    end
  end

  --- Ajoute une animation
  -- L'animation est ajoutée à l'attribut animations, indexée par le nom de l'animation.
  -- @param pFolder le chemin du sous répertoire contenant les images de l'animation
  -- @param pName le nom de l'animation
  -- @param pImageList un tableau de chaînes de caractère contenant le nom des images
  function self.addAnimation (pFolder, pName, pImageList)
    --debugFunctionEnter("sprite.addAnimation )", pFolder, pName, pImageList)
    self.addImages(pFolder, pImageList)
    animations[pName] = pImageList
  end

  --- Joue une animation
  -- @param pName le nom de l'animation
  function self.playAnimation (pName)
    --debugFunctionEnter("sprite.playAnimation )", pName)
    if (currentAnimation ~= pName) then
      currentAnimation = pName
      frame = 1
    end
  end

  --- actualise l'animation en cours
  -- @param pDt delta time
  function self.updateAnimation (pDt)
    --debugFunctionEnter("sprite.updateAnimation )", pDt) --ATTENTION cet appel peut remplir le log
    if (currentAnimation ~= SPRITE_ANIM_NONE and animations[currentAnimation] ~= nil) then
      animationTimer = animationTimer - pDt
      if (animationTimer <= 0) then
        frame = frame + 1
        animationTimer = animationSpeed
        if (frame > #animations[currentAnimation]) then
            -- est-on à la dernière frame de l'animation de destruction du sprite ?
          if (currentAnimation == SPRITE_ANIM_DESTROY) then
            -- oui, on le supprime
            self.clean()
          else
            -- non, on passe à la frame suivante
            frame = 1
          end
        end
      end
    end
  end

  --- efface tous les sprites de la liste sauf le joueur
  function self.resetAll()
    if (spriteList ~= nil) then
      local indexSprite, sprite
      for indexSprite = #spriteList, 1, -1 do
        sprite = spriteList[indexSprite]
        if (sprite.get_type() ~= SPRITE_TYPE_PLAYER) then
          sprite.clean()
          --table.remove(spriteList, indexSprite)
        end
      end
    end
  end

 --- Actualise la position du sprite en tant que joueur en fonction des entrées
  -- @param pDt delta time
  function self.updatePlayer (pDt)
    --debugFunctionEnter("sprite.updatePlayer )", pDt) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(player, nil, "sprite.updatePlayer:player", true)

    -- IMPORTANT
    -- ON GARDE ICI LES APPELS AUX GETTERS ET SETTERS DE LA CLASSE ELLE MÊME (SELF) POUR FACILITER UN ÉVENTUEL DÉPLACEMENT DU CODE DANS UN AUTRE PSUEDO OBJET.
    -- IL SUFFIRA PAR EXEMPLE DE REMPLACER SELF PAR SUPER POUR L'UTILISER DANS UNE CLASSE ENFANT
    -- rappel : l'objet player est un enfant de l'objet sprite et figure dans la liste des sprites (spriteList)

    local newAnimation = SPRITE_ANIM_IDLE

    -- BLOC PLAY_MAP
    -- déplacement avec le clavier
    if (appState.get_currentState() == SCREEN_PLAY) then
      if (player.get_moveWithKeys()) then
        -- quel type de map ?
        if (map.get_type() == MAP_TYPE_NOT_TILED) then
          -- sans tuile, on effectue des déplacements "normaux", sans containtes
          if (love.keyboard.isDown(player.get_keys()["moveUp"]) or love.keyboard.isDown(player.get_keys()["moveUpAlt"])) then
            vY = (vY - speed * pDt)
          end
          if (love.keyboard.isDown(player.get_keys()["moveDown"]) or love.keyboard.isDown(player.get_keys()["moveDownAlt"])) then
            vY = (vY + speed * pDt)
          end
          if (love.keyboard.isDown(player.get_keys()["moveLeft"]) or love.keyboard.isDown(player.get_keys()["moveLeftAlt"])) then
            vX = (vX - speed * pDt)
          end
          if (love.keyboard.isDown(player.get_keys()["moveRight"]) or love.keyboard.isDown(player.get_keys()["moveRightAlt"])) then
            vX = (vX + speed * pDt)
          end
          
          -- limite la vélocité en X
          if (vX < -velocityMax) then
            vX = -velocityMax
          end
          if (vX > velocityMax) then
            vX = velocityMax
          end
          -- limite la vélocité en Y, uniquement sur les cartes sans tuiles pour permettre les sauts et les chutes
          if (vY < -velocityMax) then
            vY = -velocityMax
          end
          if (vY > velocityMax) then
            vY = velocityMax
          end
        else
          -- avec tuile, on effectue des déplacements en fonction des tuiles autour du joueur
          -- Id des  Tuiles sous le joueur
          local idUnder = map.getTileAt(x + map.get_tileWidth() / 2, y + map.get_tileHeight())
          local idOverlap = map.getTileAt(x + map.get_tileWidth() / 2, y + map.get_tileHeight() - 1)

          -- Vérifie si le joueur a fini de sauter ou s'il peut descendre à une echelle
          if (isJumping and (self.collideBottom() or map.isLadder(idUnder))) then
            isJumping = false
            --isStanding = true
            --self.alignOnLine()
          end
          
          -- Touches droite et gauche du Clavier
          if (love.keyboard.isDown(player.get_keys()["moveRight"]) or love.keyboard.isDown(player.get_keys()["moveRightAlt"])) then
            vX = vX + speed * pDt
            if (vX > velocityMax) then vX = velocityMax end
            flip = false
            newAnimation = SPRITE_ANIM_RUN
          end
          if (love.keyboard.isDown(player.get_keys()["moveLeft"]) or love.keyboard.isDown(player.get_keys()["moveLeftAlt"])) then
            vX = vX - speed * pDt
            if (vX < -velocityMax) then vX = -velocityMax end
            flip = true
            newAnimation = SPRITE_ANIM_RUN
          end

          -- Vérifie si le joueur est sur une échelle
          local isOnLadder = map.isLadder(idUnder) or map.isLadder(idOverlap)
          
          if (not map.isLadder(idOverlap) and map.isLadder(idUnder)) then
            isStanding = true
          end
          if (map.isLadder(idOverlap) and map.isLadder(idUnder)) then
            newAnimation = SPRITE_ANIM_CLIMB_IDLE
          end

          -- Touches haut : Sauter
          if ((love.keyboard.isDown(player.get_keys()["moveUp"]) or love.keyboard.isDown(player.get_keys()["moveUpAlt"])) and isStanding and canJump and not map.isLadder(idOverlap)) then
            isJumping = true
            gravity = startGravity
            vY = velocityJump
            isStanding = false
            canJump = false
          end

          -- Vérifie si le joueur grimpe
          if (isOnLadder and not isJumping) then
            gravity = 0
            vY = 0
            canJump = false
          end
          
           -- Touches haut : Monter à l'échelle
          if ((love.keyboard.isDown(player.get_keys()["moveUp"]) or love.keyboard.isDown(player.get_keys()["moveUpAlt"])) and isOnLadder and not isJumping) then
            vY = (-math.floor(startSpeed / 4)) --on grimpe 2 fois moins vite que la vitesse normale
            newAnimation = SPRITE_ANIM_CLIMB
          end
          
           -- Touches bas : Descendre à l'échelle
          if ((love.keyboard.isDown(player.get_keys()["moveDown"]) or love.keyboard.isDown(player.get_keys()["moveDownAlt"])) and isOnLadder) then
            vY = (math.floor(startSpeed / 4)) --on descend 2 fois moins vite que la vitesse normale
            newAnimation = SPRITE_ANIM_CLIMB
          end
          
          -- Vérifie si le joueur ne grimpe plus
          if (not isOnLadder and gravity == 0 and not isJumping) then
            gravity = startGravity
          end

          -- Vérifie si le joueur est prêt pour le saut suivant ?
          if (not (love.keyboard.isDown(player.get_keys()["moveUp"]) and not love.keyboard.isDown(player.get_keys()["moveUpAlt"])) and not canJump and isStanding) then
            canJump = true
          end
        end
      end
    end --if (appState.get_currentState() == SCREEN_PLAY) then

    self.playAnimation(newAnimation)
    -- FIN BLOC PLAY_MAP
  end

 --- Actualise la position du sprite en tant que PNJ
  -- @param pDt delta time
  function self.updatePNJ (pDt)
    --debugFunctionEnter("sprite.updatePNJ )", pDt) --ATTENTION cet appel peut remplir le log
    if (type == SPRITE_TYPE_PNJ__BLOB) then 
      -- mouvement pour les sprite de type SPRITE_TYPE_PNJ__BLOB
      if (direction == SPRITE_MOVE_RIGHT) then
        vX = speed
      elseif (direction == SPRITE_MOVE_LEFT) then
        vX = -speed
      end
    end
  end

  --- Déplace, vérifie les collisions et adapte le mouvement en conséquence (Override)
  -- En cas de collision, rectifie les attributs x et y de l'objet
  -- @param pDt delta time
  -- @param pCollisionXmin (OPTIONNEL) dimension X minimale à vérifier
  -- @param pCollisionXmax (OPTIONNEL) dimension X maximale à vérifier
  -- @param pCollisionYmin (OPTIONNEL) dimension Y minimale à vérifier
  -- @param pCollisionYmax (OPTIONNEL) dimension Y maximale à vérifier
  -- @return true si il y a une collision ou false sinon
  function self.CCD(pDt, pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax)
    --debugFunctionEnter("sprite.CCD:", pDt, pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "sprite.collide:viewport", true)
    if (pCollisionXmin == nil) then pCollisionXmin = math.floor(viewport.Xmin / SCREEN_SCALE_X) end
    if (pCollisionXmax == nil) then pCollisionXmax = math.floor((viewport.Xmax - width) / SCREEN_SCALE_X) end
    if (pCollisionYmin == nil) then pCollisionYmin = math.floor(viewport.Ymin / SCREEN_SCALE_Y) end
    if (pCollisionYmax == nil) then pCollisionYmax = math.floor((viewport.Ymax - height) / SCREEN_SCALE_Y) end
    
    debugVar = self.tostring()
    
    local spriteIsPNJ = (type:find(SPRITE_TYPE_PNJ) ~= nil)
    local mapIsTiled = (map ~= nil and map.get_grid() ~= nil and map.get_type() ~= MAP_TYPE_NOT_TILED) 
    
    if (state == SPRITE_STATE_REMOVED) then return end
    
    if (appState.get_currentState() ~= SCREEN_PLAY) then return end

    --------------
    --- Actualise l'objet (remplace le self.update)
    
    -- Animation
    -- si une animation pour la destruction du sprite existe, on la joue
    if (state == SPRITE_STATE_REMOVING and animations[SPRITE_ANIM_DESTROY] == nil) then
      self.clean()
    else
      self.updateAnimation(pDt)
    end
    -- updates spécifiques au type de sprite
    if (type == SPRITE_TYPE_PLAYER) then 
      -- traite les inputs et les collisions spécifiques au joueur
      self.updatePlayer(pDt) -- IDENTIQUE 
    elseif (spriteIsPNJ) then 
      -- traite les collisions spécifiques au joueur
      self.updatePNJ(pDt)
    end
    -- fin Actualise l'objet 
    --------------
    
    if (firstUpdateDone) then
    
      -- COLLISIONS INDEPENDANTES DU TYPE DE SPRITE
      ---------------------------------------------------------
        -- DEBUT CCD
        -- on utilise la CCD pour palier au problèmes de LAG
        --------------
      
        local step = 0.05 -- TODO: vérifier si cette valeur a besoin d'être aussi petite

         -- on calcule les distance depuis le dernier update
        local distanceX = vX * pDt
        local distanceY = vY * pDt
        
        -- si le sprite (autre que le joueur) n'a ni gravité, ni poids, il ne se déplace pas en Y
        if (type ~= SPRITE_TYPE_PLAYER and (gravity == 0 or weight == 0)) then distanceY = 0 end
                   
        -- Collision detection using simple and not optimized CCD
        ---------------------------------------------------------
        
        local isCollision = false

        -- CCD Horizontale
        ----------------
        
        -- distance à tester
        local destX = x + distanceX      
        if (distanceX > 0) then
          -- vérification des collisions vers la Droite, on boucle de la position enregistrée à celle actualisée avec le pDt
          while (x < destX) do
            if (not spriteIsPNJ) then isCollision = self.collideRight() else isCollision = false end --permet de laisser faire une détection des collision spécifiques aux PNJ
            if (not isCollision) then
              -- on vérifie si un PNJ ne collisionne pas avec autre chose
              isCollision = self.collidePNJ(pDt)
            end
            if (isCollision) then
              -- collision, annule la vitesse en X
              vX = 0
              self.alignOnColumn() --?
              --  quitte la boucle
              break
            else
              -- pas de collision, on passe à la position suivante (INCREMENTATION)
              x = x + step
            end
          end
        end
        if (distanceX < 0) then
          -- vérification des collisions vers la Gauche, on boucle de la position enregistrée à celle actualisée avec le pDt
          while (x > destX) do
            if (not spriteIsPNJ) then isCollision = self.collideLeft() else isCollision = false end --permet de laisser faire une détection des collision spécifiques aux PNJ
            if (not isCollision) then
              -- on vérifie si un PNJ ne collisionne pas avec autre chose
              isCollision = self.collidePNJ(pDt)
            end
            if (isCollision) then
              -- collision, annule la vitesse en X
              vX = 0
              self.alignOnColumn() --?
              --  quitte la boucle
              break
            else
              -- pas de collision, on passe à la position suivante (DECREMENTATION)
              x = x - step
            end
          end
        end
        
        -- CCD Verticale
        ----------------
        -- distance à tester
        local destY = y + distanceY
        if (distanceY < 0) then
          -- vérification des collisions vers le haut, on boucle de la position enregistrée à celle actualisée avec le pDt
          while (y > destY) do
            if (not spriteIsPNJ) then isCollision = self.collideTop() else isCollision = false end --permet de laisser faire une détection des collision spécifiques aux PNJ
            if (not isCollision) then
              -- on vérifie si un PNJ ne collisionne pas avec autre chose            
              isCollision = self.collidePNJ(pDt)
            end
            if (isCollision) then
              -- collision, annule la vitesse en Y
              vY = 0
              --self.alignOnLine() --?
              --  quitte la boucle
              break
            else
              -- pas de collision, on passe à la position suivante (DECREMENTATION)            
              y = y - step
            end
          end
        end
        
        -- on vérifie si le sprite est en train de tomber
        if (isStanding or vY > 0) then
          if (not spriteIsPNJ) then isCollision = self.collideBottom() else isCollision = false end --permet de laisser faire une détection des collision spécifiques aux PNJ
          if (isCollision) then
            isStanding = true
            vY = 0
            self.alignOnLine()
          else
            if (gravity ~= 0) then 
              isStanding = false 
            end
          end
        end
        
        if (distanceY > 0) then
          -- vérification des collisions vers le bas, on boucle de la position enregistrée à celle actualisée avec le pDt
          while (y < destY) do
            if (not spriteIsPNJ) then isCollision = self.collideBottom() else isCollision = false end --permet de laisser faire une détection des collision spécifiques aux PNJ
            if (not isCollision) then
              -- on vérifie si un PNJ ne collisionne pas avec autre chose                        
              isCollision = self.collidePNJ(pDt)
            end
            if (isCollision) then
              -- collision, annule la vitesse en Y            
              vY = 0
              isStanding = true
              --  quitte la boucle
              break
            else
              -- pas de collision, on passe à la position suivante (INCREMENTATION)
              y = y + step
            end
          end
        end
          
   	-- Chute du Sprite, tient compte de la gravité et du poids
      if (not isStanding) then
          if (gravityModularity <= 0) then
          -- le déplacement du à la gravité est fluide
          vY = vY + (weight * gravity * pDt)
        else
          -- le déplacement du à la gravité se fait avec de la modularité
          timerDrop = timerDrop + (weight * gravity * pDt)
          if (timerDrop >= gravityModularity) then
            timerDrop = 0
            vY = vY + gravityModularity
          end
        end
      end

      if (moveWithVelocity) then
        -- Calcul des vélocités
        -----------------------
        local rotationRadian = math.rad(rotation)
        local coeff = 100 * speed * pDt
        local implusionX = math.cos (rotationRadian) * coeff
        local implusionY = math.sin (rotationRadian) * coeff
        --[[ TODO: corriger l'utilisation de l'angle de rotation qui ne sert à rien pour le moment
        vX = vX + implusionX
        vY = vY + implusionY
        ]]

        -- prise en compte de la friction de l'environnement et du poids
        local frictionAndWeight = weight * friction * pDt
        if (vX > 0) then
          vX = vX - frictionAndWeight
        elseif (vX < 0) then
          vX = vX + frictionAndWeight
        end
        if (vY > 0) then
          vY = vY - frictionAndWeight
        elseif (vY < 0) then
          vY = vY + frictionAndWeight
        end
      end --if (moveWithVelocity) then
      -- FIN CCD

      -- Collision basique avec les bord de la zone de jeu
      ---------------------------------------------------
      if (not isCollision) then 
        if (x < pCollisionXmin) then x = pCollisionXmin end
        if (y < pCollisionYmin) then y = pCollisionYmin end
        if (x > pCollisionXmax) then x = pCollisionXmax end
        if (y > pCollisionYmax) then y = pCollisionYmax end
      end
      
      -- limite la vélocité en X
      if (vX < -velocityMax) then
        vX = -velocityMax
      end
      if (vX > velocityMax) then
        vX = velocityMax
      end
      if (map.get_type() == MAP_TYPE_NOT_TILED) then
        -- limite la vélocité en Y, uniquement sur les cartes sans tuiles pour permettre les sauts et les chutes
        if (vY < -velocityMax) then
          vY = -velocityMax
        end
        if (vY > velocityMax) then
          vY = velocityMax
        end
      end
      
      ----------
      
      -- COLLISIONS DEPENDANTES DU TYPE DE SPRITE
      ---------------------------------------------------------
      -- BLOC SPR_COL
      -- on vérifie si ce sprite ne collisionne pas avec le joueur 
      local playerCollision = (state == SPRITE_STATE_NORMAL and self.boxCollision(player))
      
      if (type == SPRITE_TYPE_PROP__COIN) then
        if (playerCollision) then 
          -- une pièce collisionne avec le joueur
          isCollision = true
          indexSprite = tableIndex  -- on definit l'index a supprimer dans la table des sprite
          state = SPRITE_STATE_REMOVING --IMPORTANT
          self.destroy(spriteList, indexSprite)
          map.add_coinCount(-1)

          if (map.get_coinCount() <= 0) then
            map.set_isDoorOpen(true)
            -- parcours tous les sprites pour ouvrir les portes
            for indexSprite2 = #spriteList, 1, -1 do
              sprite2 = spriteList[indexSprite2]
              if (sprite2.get_type() == SPRITE_TYPE_ELT__DOOR) then
                sprite2.playAnimation(SPRITE_ANIM_OPEN)
              end
            end
          end
        end
      elseif (type == SPRITE_TYPE_ELT__DOOR) then
        isCollision = true
        -- une porte ouverte collisionne avec le joueur
        if (playerCollision and map.get_isDoorOpen() == true) then nextLevel() end
      elseif (type:find(SPRITE_TYPE_PNJ) ~= nil) then
        if (playerCollision) then 
          -- un PNJ collisionne avec le joueur, le joueur perd une vie
          isCollision = true
          loseLife()
        else
          -- verification des collisions du PNJ avec autre chose que le joueur
          isCollision = isCollision or self.collidePNJ()
        end
      end
    -- FIN BLOC SPR_COL
    end -- if (firstUpdateDone) then

    firstUpdateDone = true
    
    return isCollision
  end --self.CCD
  
  --- définit les hotSpots du joueur pour les collisions
  -- NOTE : remplace les hotspots de collision du sprite
  function self.createCollisionBox (pModel)
    debugFunctionEnter("sprite.createCollisionBox:", pModel)
    if (pModel == nil) then pModel = COLLISION_MODEL_POINT end
    hotSpots = {}
    if (pModel == COLLISION_MODEL_BOX6) then 
    -- modèle de collision avec 6 hotspots : top right, top left, middle right, middle left, bottom right, bottom left
      self.addHotSpot(width, 3, HOTSPOT_RIGHT, "RT")
      self.addHotSpot(width, height - 2, HOTSPOT_RIGHT, "RB")
      self.addHotSpot(-1, 3, HOTSPOT_LEFT, "LT")
      self.addHotSpot(-1, height - 2, HOTSPOT_LEFT, "LB")
      self.addHotSpot(1, height, HOTSPOT_BOTTOM, "BL")
      self.addHotSpot(width - 2, height, HOTSPOT_BOTTOM, "BR")
      self.addHotSpot(1, -1, HOTSPOT_TOP, "TL")
      self.addHotSpot(width - 2, -1, HOTSPOT_TOP, "TR")
    elseif (pModel == COLLISION_MODEL_BOX4) then
    --  modèle de collision avec 4 hotspots, middle top, middle right, middle bottom, middle left
      self.addHotSpot(width / 2 , 0 , HOTSPOT_TOP, "MT")
      self.addHotSpot(width, height / 2, HOTSPOT_RIGHT, "MR")
      self.addHotSpot(0, height / 2, HOTSPOT_LEFT, "ML")
      self.addHotSpot(width / 2 , height, HOTSPOT_BOTTOM, "MB")
    elseif (pModel == COLLISION_MODEL_POINT) then 
    --  modèle de collision avec 1 hotspots: centre 
      self.addHotSpot(width / 2 , height / 2 , HOTSPOT_UNIQUE, "CE")
    elseif (pModel == COLLISION_MODEL_NONE) then 
    --  pas de modèle de collision , aucun de hotspot
    end
  end

  -- initialisation par défaut
  self.initialize()
  -- Ajout du sprite à la liste
  if (spriteList ~= nil) then
    -- index du sprite dans la table (utile pour sa suppression)
    self.set_tableIndex (#spriteList + 1) 
    table.insert(spriteList, self)
  end

  return self
end