-- Regroupe des fonctions utilitaires
--------------------------------

--- Remplace l'instruction switch
--[[
  USAGE:
  switch(case) {
    [1] = function () print"number 1!" end,
    [2] = math.sin,
    [false] = function (a) return function (b) return (a or b) and not (a and b) end end,
    Default = function (x) print"Look, Mom, I can differentiate types!" end, -- ["Default"] ;)
    [Default] = print,
    [Nil] = function () print"I must've left it in my other jeans." end,
  }
]]
Default, Nil = {}, function () end --- for uniqueness
function switch (pIndex)
  return setmetatable({pIndex}, {
    __call = function (t, cases)
      local item = #t == 0 and Nil or t[1]
      return (cases[item] or cases[Default] or Nil)(item)
    end
  })
end

--- Itérateur pour une table FINIE
--[[
USAGE 1:
  t = {10, 20, 30}
  iter = iterator(t)    -- creates the iterator
  while true do
    local element = iter()   -- calls the iterator
    if element == nil then break end
    print(element)
  end
USAGE 2:
  t = {10, 20, 30}
  for element in iterator(t) do
    print(element)
  end
]]
function iterator (pTable)
  local i = 0
  local n = table.getn(pTable)
  return function ()
    i = i + 1
    if i <= n then return pTable[i] end
  end
end

--- Mélange le contenu d'une table
-- @param pTable table à mélanger
-- @see https://forums.coronalabs.com/topic/195-random-table-shuffle/
function shuffle (pTable)
  local count = #pTable
  math.randomseed(love.timer.getTime())
  for i = 1, (count * 20) do
    local index0 = math.random(1, count)
    local index1 = math.random(1, count)
    local temp = pTable [index0]
    pTable[index0] = pTable [index1]
    pTable[index1] = temp
  end
  return pTable
end

--- Affiche le contenu d'une table
-- @param pVar variable à afficher
function dump (pVar)
  if (pVar == nil) then
    print "NIL"
  else
    for k, v in pairs (pVar) do
      print(tostring(k), " = ", tostring(v))
    end
  end
end

--- Retourne TRUE si une une variable est égale à une valeur donnée, FALSE sinon et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
-- @return true si l'assertion est vraie, false sinon
function assertEqual (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
      print ("assertEqual: ", pMessage, " Value=", tostring(pValue))
    end
    return true
  else
    return false
  end
end

--- Quitte l'application si une une variable est égale à une valeur donnée, et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
function assertEqualQuit (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
       print ("assertEqualQuit: ", pMessage, " Value=", tostring(pValue))
    end
    print ("FIN DE L'APPLICATION SUR VALIDATION D'UNE ASSERTION")
    love.event.push('quit')
  end
end

--- Logue (Affiche) un message d'erreur
-- @param pMessage texte à loguer
-- @param mustQuit (OPTIONNEL) si vrai, termine l'application après avoir afficher le message
function logError (pMessage, pMustQuit)
  print (pMessage)
  if (pMustQuit) then love.window.close() end
end

--- Logue (Affiche) des contenus pour du debug
-- @param Paramètres dynamiques
function debugMessage (...)
  if (DEBUG_MODE >= 1) then
    print(os.date().."# INFO #", ...)
  end
end

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction
-- @param Paramètres dynamiques
function debugFunctionEnter (...)
  if (DEBUG_MODE >= 3) then
    print(os.date().."# DEBUG #", ...)
  end
end

---Affiche un point rouge au coordonnées données
--@param pX position X du point
--@param pY position Y du point
function debugPoint (pX, pY)
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle("fill", pX, pY, 10, 10)
  love.graphics.setColor(255, 255, 255, 255)
end

--- Logue (Affiche) des contenus d'information
-- @param Paramètres dynamiques
function logMessage (...)
  print(os.date().."# LOG #", ...)
end