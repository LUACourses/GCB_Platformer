-- Regroupe les constantes nécessaires au framework
-- Elles ne devraient pas être modifiées pendant l'éxécution
--------------------------------

-- Définit des niveau d'affichage des information de debug (affiche des infos supplémentaires)
-- 0: aucun
-- 1: messages affichés via la fonction debugMessage
-- 2: comme précédent mais affiche aussi les infos de debug dans le HUD (variable debugInfos)
-- 3: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnter
-- 4: comme précédent mais affiche aussi les identifiants des sprites
DEBUG_MODE = 0
-- position X de la fenêtre de l'application en mode DEBUG
DEBUG_WINDOWS_X = 100
-- position Y de la fenêtre de l'application en mode DEBUG
DEBUG_WINDOWS_Y = 100
-- écran sur lequel est positionné la fenêtre de l'application en mode DEBUG
DEBUG_DISPLAY_NUM = 2
--DEBUG_DISPLAY_NUM = love.window.getDisplayCount() -- le dernier écran
-- permet d'afficher les éléments invisibles de la carte
DEBUG_SHOW_MAP_INVISIBLE = false

-- nom des différents écrans de l'application
SCREEN_PLAY  = "screen_play"
SCREEN_PAUSE = "screen_pause"
SCREEN_START = "screen_start"
SCREEN_END   = "screen_end"
SCREEN_NEXT  = "screen_next"

INVALID_OR_TEST_VALUE = "invalid_or_test_value" -- pour des tests uniquement

-- Facteur d'échelle pour l'affichage en X
SCREEN_SCALE_X = 3

-- Facteur d'échelle pour l'affichage en Y
SCREEN_SCALE_Y  = SCREEN_SCALE_X