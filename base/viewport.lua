-- Entité représentant la zone de jeu
--------------------------------
require("base/functions")

--- Crée un pseudo objet de type viewport
-- @return un pseudo objet viewport
function newViewport ()
  local self = {}

  --- Initialise l'objet
  function self.initialize ()
    debugFunctionEnter("viewport.initialize")

    -- position X minimale
    self.Xmin = 0
    -- position X maximale
    self.Xmax = love.graphics.getWidth()
    -- position Y minimale
    self.Ymin = 0
    -- position Y maximale
    self.Ymax = love.graphics.getHeight()
    -- largeur de caractère
    self.charWidth  = 10
    -- hauteur de caractère
    self.charHeight = 15
    -- couleur de fond par défaut
    self.backgroundColor = {0, 0, 50}
    -- couleur de texte par défaut
    self.textColor = {255, 255, 255}
    -- le viewport a t'il déjà été dessiné ?
    self.firstDrawDone = false

    -- cache le curseur de la souris
    love.mouse.setVisible(false)
    -- par défaut confine la souris dans la fenêtre, sauf en mode debug
    if (DEBUG_MODE < 0) then love.mouse.setGrabbed(true) end
  end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_width ()
    return math.floor((self.Xmax - self.Xmin) / SCREEN_SCALE_X)
  end
  function self.get_height ()
    return math.floor((self.Ymax - self.Ymin) / SCREEN_SCALE_Y)
  end

  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("viewport.destroy")
    self.clean()
    self = nil
  end

  --- Effectue des nettoyages en quittant le jeu
  function self.clean ()
    debugFunctionEnter("viewport.clean")
    -- affiche le curseur de la souris
    love.mouse.setVisible(true)
  end

  --- Dessine l'objet
  function self.draw ()
    --debugFunctionEnter("viewport.draw") --ATTENTION cet appel peut remplir le log
    -- utilise la couleur de fond définie pour remplir la zone de jeu
    love.graphics.setColor(self.backgroundColor[1], self.backgroundColor[2], self.backgroundColor[3])
    love.graphics.rectangle("fill", self.Xmin / SCREEN_SCALE_X, self.Ymin / SCREEN_SCALE_Y, self.Xmax / SCREEN_SCALE_X, self.Ymax / SCREEN_SCALE_Y)
    love.graphics.setColor(self.textColor[1], self.textColor[2], self.textColor[3])
    love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
    self.firstDrawDone = true
  end

  -- initialisation par défaut
  self.initialize()
  return self
end
