-- Entité gérant l'affichage en général
--------------------------------
require("base/functions")

-- positions relatives utilisées par la fonction displayText()
POS_TOP_LEFT      = "pos_top_left"
POS_TOP_RIGHT     = "pos_top_right"
POS_TOP_CENTER    = "pos_top_center"
POS_CENTER        = "pos_center"
POS_CENTER_LEFT   = "pos_center_left"
POS_CENTER_RIGHT  = "pos_center_right"
POS_BOTTOM_LEFT   = "pos_bottom_left"
POS_BOTTOM_RIGHT  = "pos_bottom_right"
POS_BOTTOM_CENTER = "pos_bottom_center"

-- animation possibles pour les textes utilisées par la fonction animText()
TEXT_ANIM_SINUS_CENTER = "text_anim_sinus_center"
TEXT_ANIM_SCROLL_SINUS_HORZ = "text_anim_scroll_sinus_horz"
TEXT_ANIM_SCROLL_SINUS_VERT = "text_anim_scroll_sinus_vert"
TEXT_ANIM_SCROLL_HORZ = "text_anim_scroll_horz"

-- type de fonts disponibles
FONT_MENUTITLE = "font_menutitle"
FONT_MENUCONTENT = "font_menucontent"
FONT_CONTENT = "font_content"
FONT_GUI = "font_gui"
FONT_NORMAL = "font_normal"
FONT_DEBUG = "font_debug"

--- Crée un pseudo objet de type HUD
-- @return un pseudo objet HUD
function newHUD ()
  local self = {}

  -- polices (fonts) utilisées par les différents affichages
  local fonts = {}

  -- dernier screen dessiné
  local lastDrawScreen = nil

  --- Initialise l'objet
  function self.initialize ()
    debugMessage("HUD.initialize")

    -- Note: la taille des polices tient compte de l'échelle de l'écran
    -- police pour les titres dans les menus
    fonts[FONT_MENUTITLE] = love.graphics.newFont("fonts/menuTitle.ttf", math.floor(50 / SCREEN_SCALE_X))
    -- police pour le contenu dans les menus
    fonts[FONT_MENUCONTENT] = love.graphics.newFont("fonts/menuContent.ttf", math.floor(20 / SCREEN_SCALE_X))
    -- police pour les contenu (par défaut)
    fonts[FONT_CONTENT] = love.graphics.newFont("fonts/content.ttf", math.floor(20 / SCREEN_SCALE_X))
    -- police pour les GUI (score, niveau...)
    fonts[FONT_GUI] = love.graphics.newFont("fonts/gui.ttf", math.floor(30 / SCREEN_SCALE_X))
    -- police pour les affichages normaux (debug)
    fonts[FONT_NORMAL] = love.graphics.newFont("fonts/normal.ttf", math.floor(10 / SCREEN_SCALE_X))
    -- police pour les affichages normaux (debug)
    fonts[FONT_DEBUG] = love.graphics.newFont("fonts/normal.ttf", math.floor(20 / SCREEN_SCALE_X))

    love.graphics.setFont(fonts[FONT_CONTENT])
  end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_fontMenuTitle ()
    return fonts[FONT_MENUTITLE]
  end
  function self.set_fontMenuTitle (pValue)
    fonts[FONT_MENUTITLE] = pValue
  end
  function self.get_fontMenuContent ()
    return fonts[FONT_MENUCONTENT]
  end
  function self.set_fontMenuContent (pValue)
    fonts[FONT_MENUCONTENT] = pValue
  end
  function self.get_fontContent ()
    return fonts[FONT_CONTENT]
  end
  function self.set_fontContent (pValue)
    fonts[FONT_CONTENT] = pValue
  end
  function self.get_fontGUI ()
    return fonts[FONT_GUI]
  end
  function self.set_fontGUI (pValue)
    fonts[FONT_GUI] = pValue
  end
  function self.get_fontNormal ()
    return fonts[FONT_NORMAL]
  end
  function self.set_fontNormal (pValue)
    fonts[FONT_NORMAL] = pValue
  end
  function self.get_fontDebug ()
    return fonts[FONT_DEBUG]
  end
  function self.set_fontDebug (pValue)
    fonts[FONT_DEBUG] = pValue
  end
  function self.get_lastDrawScreen ()
    return lastDrawScreen
  end
  function self.set_lastDrawScreen (pValue)
    lastDrawScreen = pValue
  end

  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("HUD.destroy")
    self.clean()
    self = nil
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function self.clean ()
    debugFunctionEnter("HUD.clean")
    -- pour le moment rien à faire
  end

  --- Actualise l'objet (Override)
  -- @param pDt delta time
  function self.update (pDt)
    --debugFunctionEnter("HUD.update ", pDt) --ATTENTION  cet appel peut remplir le log
    timersForAnimation.add(pDt)
  end

  --- Dessine l'objet
  function self.draw ()
    --debugFunctionEnter("HUD.draw") --ATTENTION cet appel peut remplir le log
    --normalement cette fonction ne fait pas grand chose car les écrans sont dessinés dans les fonctions drawXXXScreen
    -- pour le moment rien à faire
  end

  --- Affiche l'écran d'erreur
  function self.drawBadScreen ()
    --debugFunctionEnter("HUD.drawBadScreen") --ATTENTION cet appel peut remplir le log
    displayText("MENU INVALIDE", fonts[FONT_CONTENT], POS_CENTER, 0, 0, {255, 0, 0, 255}) --couleur rouge sans transparence
    lastDrawScreen = appState.get_currentState()
  end

  --- Affiche un texte à l'écran en le positionnant relativement dans le viewport
  -- @param pText texte à afficher
  -- @parampFont police à utiliser
  -- @param pCONST_POSITON (OPTIONNEL) position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y
  -- @param pColor (OPTIONNEL) couleur du texte. La couleur blanc sera utilisée par défaut
  function self.displayText (pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor)
    --debugFunctionEnter("HUD.displayText:", pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor) --ATTENTION cet appel.HS peut remplir le log
    assertEqualQuit(viewport, nil, "HUD.displayText:viewport", true)
    assertEqualQuit(pText, nil, "HUD.displayText:pText", true)
    assertEqualQuit(pFont, nil, "HUD.displayText:pFont", true)

    if (pCONST_POSITON == nil) then pCONST_POSITON = POS_CENTER end
    if (pOffsetX == nil) then pOffsetX = 0 end
    if (pOffsetY == nil) then pOffsetY = 0 end
    if (pColor == nil) then pColor = {{255, 255, 255, 255}} end

    love.graphics.setColor(pColor[1], pColor[2], pColor[3], pColor[4])

    love.graphics.setFont(pFont)

    local textWidth = pFont:getWidth(pText)
    local textHeight = pFont:getHeight(pText)
    local Xmax = viewport.get_width() - textWidth
    local Ymax = viewport.get_height() - textHeight
    local drawX, drawY

    switch (pCONST_POSITON) {
      [POS_TOP_LEFT]      = function () drawX = pOffsetX           ; drawY = pOffsetY            end,
      [POS_TOP_RIGHT]     = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY            end,
      [POS_TOP_CENTER]    = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY            end,
      [POS_CENTER]        = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax / 2 end,
      [POS_BOTTOM_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_CENTER] = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax     end
    }

    -- NOTE: lors de la mise à l'échelle, il se peut que les textes affichés en bordure d'écran sortent. on corrige en les repositionnant dans les limites
    if (drawX + textWidth > Xmax ) then drawX = math.floor(Xmax - textWidth) end
    if (drawY + textHeight > Ymax ) then drawY = math.floor(Ymax - textHeight) end

    love.graphics.print(pText, drawX, drawY)
    love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
  end

 --- Affiche un texte à l'écran en le positionnant relativement dans le viewport avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pText texte à afficher
  -- @parampFont police à utiliser
  -- @param pCONST_POSITON (OPTIONNEL) position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y
  -- @param pColor (OPTIONNEL) couleur du texte. La couleur blanc sera utilisée par défaut
  -- @param pTextWidth (OPTIONNEL) largeur du texte. Si absent la valeur sera calculée automatiquement.
  -- @param pTextHeight (OPTIONNEL) hauteur du texte. Si absent la valeur sera calculée automatiquement.
  function self.displayTextScale (pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
    --debugFunctionEnter("HUD.displayTextScale:", pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)--ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "HUD.displayTextScale:viewport", true)
    assertEqualQuit(pText, nil, "HUD.displayTextScale:pText", true)
    assertEqualQuit(pFont, nil, "HUD.displayTextScale:pFont", true)
    love.graphics.setFont(pFont)

    if (pCONST_POSITON == nil) then pCONST_POSITON = POS_CENTER end
    if (pOffsetX == nil) then pOffsetX = 0 else pOffsetX = (pOffsetX / SCREEN_SCALE_X) end
    if (pOffsetY == nil) then pOffsetY = 0 else pOffsetY = (pOffsetY / SCREEN_SCALE_Y) end
    if (pColor == nil) then pColor = {{255, 255, 255, 255}} end
    if (pTextWidth == nil) then pTextWidth = pFont:getWidth(pText) else pTextWidth = (pTextWidth / SCREEN_SCALE_X) end -- note la font tient déjà compte de l'échelle
    if (pTextHeight == nil) then pTextHeight = pFont:getHeight(pText) else pTextHeight = (pTextHeight / SCREEN_SCALE_Y) end -- note la font tient déjà compte de l'échelle

    love.graphics.setColor(pColor[1], pColor[2], pColor[3], pColor[4])

    local Xmax = viewport.get_width() - pTextWidth
    local Ymax = viewport.get_height() - pTextHeight
    local drawX, drawY

    switch (pCONST_POSITON) {
      [POS_TOP_LEFT]      = function () drawX = pOffsetX           ; drawY = pOffsetY            end,
      [POS_TOP_RIGHT]     = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY            end,
      [POS_TOP_CENTER]    = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY            end,
      [POS_CENTER]        = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax / 2 end,
      [POS_BOTTOM_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_CENTER] = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax     end
    }

    -- NOTE: lors de la mise à l'échelle, il se peut que les textes affichés en bordure d'écran sortent. on corrige en les repositionnant dans les limites
    if (drawX > Xmax ) then drawX = Xmax end
    if (drawY > Ymax ) then drawY = Ymax end
    drawX = math.floor(drawX)
    drawY = math.floor(drawY)
    --debugMessage("drawX, drawY=",drawX, drawY)
    love.graphics.print(pText, drawX, drawY)
    love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
  end

  --- Affiche un texte à l'écran en utilisant une animation simple
  -- @param pText texte à afficher
  -- @param pFont police à utiliser
  -- @param pCONST_ANIM (OPTIONNEL) type d'animation à utiliser (voir les constantes TEXT_ANIM_xxx)
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X. Si absent, la valeur sera 0
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y. Si absent, la valeur sera 0
  -- @param pWidth (OPTIONNEL) largeur de l'animation. Si absent, la valeur sera la hauteur de la zone d'affichage
  -- @param pHeight (OPTIONNEL) Hauteur de l'animation. Si absent, la valeur sera la largeur de la zone d'affichage
  -- @param pColors (OPTIONNEL) tableau contenant les couleurs des lettres du texte. Si absent, la valeur sera blanc
  -- @param pTimerAnimIndex (OPTIONNEL) index du timer gérant l'animation. Si absent, la valeur sera 1
  -- @param pSpeed (OPTIONNEL) vitesse de déplacement, peut être négative pour inverser le sens de déplacement. Si absent, la valeur 50
  -- @param pXstart (OPTIONNEL) position de départ en X. Si absent, l'animation sera centrée en X
  -- @param pYstart (OPTIONNEL) position de départ en Y. Si absent, l'animation sera centrée en Y
  function self.animText (pText, pFont, pCONST_ANIM, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    --debugFunctionEnter("HUD.animText:", pText, pFont, pCONST_ANIM, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "HUD.animText:viewport", true)
    assertEqualQuit(pText, nil, "HUD.animText:pText", true)
    assertEqualQuit(pFont, nil, "HUD.animText:pFont", true)

    if (SCREEN_SCALE_X ~= 1 or SCREEN_SCALE_Y ~= 1) then
      -- dans cas ou l'échelle de l'affichage n'est pas 1, on affiche le texte sans animation car sinon il sortirait de l'écran
        HUD.displayTextScale(pText, pFont, POS_CENTER, pOffsetX, pOffsetY)
    else
      love.graphics.setFont(pFont)
      local textWidth = pFont:getWidth(pText)
      local textHeight = pFont:getHeight(pText)
      local Xmax = viewport.get_width()-- valeur max
      local Ymax = viewport.get_height() -- valeur max

      if (pCONST_ANIM == nil) then pCONST_ANIM = TEXT_ANIM_SINUS_CENTER end
      if (pOffsetX == nil) then pOffsetX = 0 end
      if (pOffsetY == nil) then pOffsetY = 0 end
      if (pWidth == nil) then pWidth = viewport.get_width() end
      if (pHeight == nil) then pHeight = viewport.get_height() end
      if (pColors == nil) then pColors = {{255, 255, 255, 255}} end
      if (pTimerAnimIndex == nil) then pTimerAnimIndex = 1 end
      if (pSpeed == nil) then pSpeed = 50 end
      if (pXstart == nil) then pXstart = (Xmax - textWidth) / 2 end -- centré en X
      if (pYstart == nil) then pYstart = (Ymax - textHeight) / 2 end -- centré en Y

      local Xdraw = 0
      local Ydraw = 0
      local colorIndex = 1
      local color, char

      switch (pCONST_ANIM) {
          [TEXT_ANIM_SINUS_CENTER] = function ()
          -- OK TEXT_ANIM_SINUS_CENTER animation du texte avec une sinusoïdale centrée
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            Ydraw = math.sin((Xdraw + timersForAnimation.val(pTimerAnimIndex) * 200) / 50) * pSpeed
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_SINUS_VERT] = function ()
          -- OK TEXT_ANIM_SCROLL_SINUS_VERT animation du texte avec un scrolling vertical sinusoïdal centrée
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            Ydraw = math.sin((timersForAnimation.val(pTimerAnimIndex) * 200) / 50) * pSpeed
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_SINUS_HORZ] = function ()
          -- OK TEXT_ANIM_SCROLL_SINUS_HORZ animation du texte avec un scrolling horizontal sinusoïdal centrée
          Xdraw = math.sin((timersForAnimation.val(pTimerAnimIndex)* 200) / 50) * pSpeed
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_HORZ] = function ()
          -- TEXT_ANIM_SCROLL_HORZ animation du texte avec un scrolling horizontal sinusoïdal centrée
          -- animation du texte avec un scrolling horizontal
          Xdraw = timersForAnimation.val(pTimerAnimIndex) * pSpeed
          if (pSpeed > 0 and (pXstart + Xdraw + pOffsetX) >= Xmax) then
            timersForAnimation.setval(pTimerAnimIndex, 0)
            Xdraw = timersForAnimation.val(pTimerAnimIndex) * pSpeed
          end
          if (pSpeed < 0 and (pXstart + Xdraw + pOffsetX) < pXstart) then
            timersForAnimation.setval(pTimerAnimIndex, 0)
            Xdraw = timersForAnimation.val(pTimerAnimIndex) * pSpeed
          end

          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            --print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY, "Xmax ", Xmax)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end
      }
    love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur
    end --if (SCREEN_SCALE_X ~= 1 or SCREEN_SCALE_Y ~= 1) then
  end

  --- Affiche une image sans mise à l'échelle
  -- @param pImage see love.graphics.draw
  -- @param pX see love.graphics.draw
  -- @param pY see love.graphics.draw
  -- @param pRotation see love.graphics.draw
  -- @param pScaleX see love.graphics.draw
  -- @param pScaleY see love.graphics.draw
  -- @param pOrigineX see love.graphics.draw
  -- @param pOrigineY see love.graphics.draw
  function self.drawImage (pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
    --debugFunctionEnter("HUD.drawImage:", pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY) --ATTENTION cet appel peut remplir le log
    if (pImage == nil) then return end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pRotation == nil) then pRotation = 0 end
    if (pScaleX == nil) then pScaleX = 1 end
    if (pScaleY == nil) then pScaleY = 1 end
    if (pOrigineX == nil) then pOrigineX = 0 end
    if (pOrigineY == nil) then pOrigineY = 0 end
    love.graphics.draw(pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
  end

  --- Affiche une image avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pImage see love.graphics.draw
  -- @param pX see love.graphics.draw
  -- @param pY see love.graphics.draw
  -- @param pRotation see love.graphics.draw
  -- @param pScaleX see love.graphics.draw
  -- @param pScaleY see love.graphics.draw
  -- @param pOrigineX see love.graphics.draw
  -- @param pOrigineY see love.graphics.draw
  -- @param offsetX (OPTIONNEL) offset en X NON MIS À L'ÉCHELLE
  -- @param offsetY (OPTIONNEL) offset en Y NON MIS À L'ÉCHELLE
  function self.drawImageScale (pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY, offsetX, offsetY)
    --debugFunctionEnter("HUD.drawImageScale:", pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY, offsetX, offsetY) --ATTENTION cet appel peut remplir le log
    if (pImage == nil) then return end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pRotation == nil) then pRotation = 0 end
    if (pScaleX == nil) then pScaleX = 1 end
    if (pScaleY == nil) then pScaleY = 1 end
    if (pOrigineX == nil) then pOrigineX = 0 end
    if (pOrigineY == nil) then pOrigineY = 0 end
    if (offsetX == nil) then offsetX = 0 end
    if (offsetY == nil) then offsetY = 0 end
    pX = math.floor(pX / SCREEN_SCALE_X)
    pY = math.floor(pY / SCREEN_SCALE_Y)
    pScaleX = pScaleX / SCREEN_SCALE_X
    pScaleY = pScaleY / SCREEN_SCALE_Y
    pOrigineX = math.floor(pOrigineX / SCREEN_SCALE_X)
    pOrigineY = math.floor(pOrigineY / SCREEN_SCALE_Y)
    love.graphics.draw(pImage, pX + offsetX, pY + offsetY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
  end

  --- Affiche une rectangle sans mise à l'échelle
  -- @param pMode see love.graphics.rectangle
  -- @param pX see love.graphics.rectangle
  -- @param pY see love.graphics.rectangle
  -- @param pW see love.graphics.rectangle
  -- @param pH see love.graphics.rectangle
  function self.rectangle (pMode, pX, pY, pW, pH)
    --debugFunctionEnter("HUD.rectangle:", pMode, pX, pY, pW, pH) --ATTENTION cet appel peut remplir le log
    if (pMode == nil) then pMode = "fill" end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pW == nil) then pW = 1 end
    if (pH == nil) then pH = 1 end
    love.graphics.rectangle(pMode, pX, pY, pW, pH)
  end

  --- Affiche une rectangle avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pMode see love.graphics.rectangle
  -- @param pX see love.graphics.rectangle
  -- @param pY see love.graphics.rectangle
  -- @param pW see love.graphics.rectangle
  -- @param pH see love.graphics.rectangle
  -- @param offsetX (OPTIONNEL) offset en X NON MIS À L'ÉCHELLE
  -- @param offsetY (OPTIONNEL) offset en Y NON MIS À L'ÉCHELLE
  function self.rectangleScale (pMode, pX, pY, pW, pH, offsetX, offsetY)
    --debugFunctionEnter("HUD.rectangleScale:", pMode, pX, pY, pW, pH, offsetX, offsetY) --ATTENTION cet appel peut remplir le log
    if (pMode == nil) then pMode = "fill" end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pW == nil) then pW = 1 end
    if (pH == nil) then pH = 1 end
    if (offsetX == nil) then offsetX = 0 end
    if (offsetY == nil) then offsetY = 0 end
    pX = math.floor(pX / SCREEN_SCALE_X)
    pY = math.floor(pY / SCREEN_SCALE_Y)
    pW = math.floor(pW / SCREEN_SCALE_X)
    pH = math.floor(pH / SCREEN_SCALE_Y)
    love.graphics.rectangle(pMode, pX + offsetX, pY + offsetY, pW, pH)
  end

  -- initialisation par défaut
  self.initialize()
  return self
end