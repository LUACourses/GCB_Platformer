-- Entité gérant les musiques
--------------------------------
require("base/functions")

--- Crée un pseudo objet de type musicManager
-- @return un pseudo objet musicManager
function newMusicManager ()
  local self = {}

  --- Initialise l'objet
  function self.initialize ()
    debugFunctionEnter("musicManager.initialize")

    -- pas pour l'augmentation automatique du volume
    self.stepUp    = 0.2 -- 1/5 de seconde
    -- pas pour la diminution automatique du volume
    self.stepDown  = 0.2 -- 1/5 de seconde
    -- index de la piste en cours
    self.trackPlayed   = 0 -- pas de morceau en cours
    -- une piste est elle en cours de lecture
    self.isPlaying = false
    -- liste des pistes (musiques) disponibles
    self.trackList = {}
  end

  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("musicManager.destroy")
    self.clean()
    self = nil
  end

  --- Effectue des nettoyages en quittant le jeu
  function self.clean ()
    debugFunctionEnter("musicManager.clean")
    -- pour le moment rien à faire
  end

  --- Met à jour les pistes à lire et applique un fading sur le volume si besoin
  -- @param pDt delta time
  function self.update (pDt)
    --debugFunctionEnter("musicManager.update )", pDt) --ATTENTION cet appel peut remplir le log
    if (self.trackPlayed < 1 or #self.trackList < 1 or not self.isPlaying) then return end
    local i
    local debug = ""
    for i = 1, #self.trackList do
      -- augmente le volume de la piste en cours s'il n'est pas déjà au maximum
      local track = self.trackList[i]
      local volume = track.audioSource:getVolume()
      debug = debug .. "track "..i
      if (i == self.trackPlayed and volume < track.maximalVolume) then
        track.audioSource:setVolume(volume + self.stepUp * pDt)
        debug = debug .. " UP "..track.audioSource:getVolume()
      end
      -- diminue le volume des pistes autres que celle en cours si leur volume n'est pas déjà nul
      if (i ~= self.trackPlayed) then
        if (volume > 0) then
          -- baisse le volume progressivement
          track.audioSource:setVolume(volume - self.stepDown * pDt)
          debug = debug .. " DW "..track.audioSource:getVolume()
        else
          -- volume égal à 0, on arrête la lecture
          track.audioSource:stop()
        end
      end
    end
  end

  --- Ajoute une piste à la liste
  -- @parampFilename nom complet du fichier audio (relatif au dossier de l'application)
  -- @param pMode (OPTIONNEL) "static" ou "stream" (voir l'option associée de la fonction love.audio.newSource)
  -- @param pMaximalVolume (OPTIONNEL) volume maximal auquel il doit être joué (entre 0 et 1)
  -- @param pIsLooping (OPTIONNEL) true pour jouer la piste en boucle
  -- @param pNoFade (OPTIONNEL) true pour indiquer que cette piste sera toujours jouée immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function self.add (pFilename, pMode, pMaximalVolume, pIsLooping, pNoFade)
    debugFunctionEnter("musicManager.add ", pFilename, pMode, pMaximalVolume, pIsLooping, pNoFade)
    if (assertEqual(pFilename, nil, "musicManager.addTrack:pFilename")) then return end
    if (pMode == nil) then pMode = "static" end
    if (pMaximalVolume == nil) then pMaximalVolume = 1 end
    if (pIsLooping == nil) then pIsLooping = true end
    if (pNoFade == nil) then pNoFade = false end

    local newTrack = {}
    newTrack.audioSource = love.audio.newSource(pFilename, pMode)
    newTrack.mode = pMode
    newTrack.maximalVolume = pMaximalVolume
    newTrack.isLooping = pIsLooping
    newTrack.noFade = pNoFade
    table.insert(self.trackList,newTrack)
  end

  --- Lit la piste demandée
  -- @parampTrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function self.play (pTrackIndex, pVolume, pNoFade)
    debugFunctionEnter("musicManager.play ", pTrackIndex, pVolume, pNoFade)
    if (#self.trackList < 1) then return end

    if (pTrackIndex == nil) then pTrackIndex = self.trackPlayed end
    if (pTrackIndex > #self.trackList) then pTrackIndex = #self.trackList end

    local track = self.trackList[pTrackIndex]
    if (pVolume ~= nil) then
      -- si le volume a été fixé, alors on change le volume maximal de la piste
      track.maximalVolume = pVolume
    end
    track.audioSource:setLooping(track.isLooping)
    if (pNoFade) then
      -- si la lecture est immédiate (nofade), on fixe le volume des pistes sans attendre l'update
      self.trackPlayed.audioSource:setVolume(0) -- courante à 0
      track.audioSource:setVolume(pVolume) -- nouvelle piste au volume max
    else
      -- sinon on débute avec un volume = 0
      track.audioSource:setVolume(0)
    end
    self.trackPlayed = pTrackIndex
    track.audioSource:play()

    self.isPlaying = true
  end

  --- Met la piste courante en pause
  function self.pause ()
    debugFunctionEnter("musicManager.pause")
    if (#self.trackList < 1 or self.trackPlayed < 1) then return end
    local track = self.trackList[self.trackPlayed]
    if (track ~= nil) then
      if (track.audioSource:isPlaying()) then
        track.audioSource:pause()
      end
    end
    self.isPlaying = false
  end

  --- Reprend la lecture de la piste courante
  function self.resume ()
    debugFunctionEnter("musicManager.resume")
    if (#self.trackList < 1 or self.trackPlayed < 1) then return end
    local track = self.trackList[self.trackPlayed]
    if (track ~= nil) then
      if (not track.audioSource:isPlaying()) then
        track.audioSource:resume()
      end
    end
    self.isPlaying = true
  end

  --- Arrête la lecture de la piste courante
  function self.stop ()
    debugFunctionEnter("musicManager.stop")
    if (#self.trackList < 1 or self.trackPlayed < 1) then return end
    local track = self.trackList[self.trackPlayed]
    if (track ~= nil) then
      track.audioSource:stop()
    end
    self.isPlaying = true
  end

  --- Lit la piste suivante
  -- @paramtrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function self.playNext (pVolume, pNoFade)
    debugFunctionEnter("musicManager.playNext ", pVolume, pNoFade)
    local index = self.trackPlayed + 1
    if (index > #self.trackList) then index = 1 end
    self.play(index, pVolume, pNoFade)
  end

  --- Lit la piste précédente
  -- @paramtrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function self.playPrevious (pVolume, pNoFade)
    debugFunctionEnter("musicManager.playPrevious ", pVolume, pNoFade)
    local index = self.trackPlayed - 1
    if (index < 1) then index = #self.trackList end
    self.play(index, pVolume, pNoFade)
  end

  -- initialisation par défaut
  self.initialize()
  return self
end