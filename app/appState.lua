-- Entité représentant l'état de l'application à un instant donné
--------------------------------
require("base/functions")

---- Crée un pseudo objet de type appState
-- @return un pseudo objet appState
function newAppState ()
  local self = {}

  -- État courant de l'application
  local currentState
  -- l'application a t-elle démarrée ?
  local hasStarted
  -- nombre de niveaux à atteindre pour gagner le jeu
  local levelToWin
  -- nombre de points à marquer pour passer un niveau
  local pointForLevel

  --- Initialise l'objet
  -- @param pLevelToWin (OPTIONNEL) niveau à atteindre pour gagner le jeu
  -- @param pPointForLevel (OPTIONNEL) nombre de points à marquer pour passer un niveau
  function self.initialize (pLevelToWin, pPointForLevel)
    debugFunctionEnter("appState.initialize:", pLevelToWin, pPointForLevel)
    if (pLevelToWin == nil) then pLevelToWin = 1 end
    if (pPointForLevel == nil) then pPointForLevel = 10 end

    currentState   = SCREEN_START
    --currentState   = SCREEN_PLAY -- DEBUG uniquement 
    hasStarted = false
    levelToWin     = pLevelToWin
    pointForLevel  = pPointForLevel
  end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_currentState ()
    return currentState
  end
  function self.set_currentState (pValue)
    currentState = pValue
  end
  function self.get_hasStarted ()
    return hasStarted
  end
  function self.set_hasStarted (pValue)
    hasStarted = pValue
  end
  function self.get_levelToWin ()
    return levelToWin
  end
  function self.set_levelToWin (pValue)
    levelToWin = pValue
  end
  function self.get_pointForLevel ()
    return pointForLevel
  end
  function self.set_pointForLevel (pValue)
    pointForLevel = pValue
  end

  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("appState.destroy")
    self.clean()
    self = nil
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function self.clean ()
    -- pour le moment rien à faire
  end

  -- initialisation par défaut
  self.initialize()
  return self
end