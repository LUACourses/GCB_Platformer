-- Entité regroupant les paramètres de l'application
--------------------------------

function newSettings ()
  local self = {}

  --- Initialise l'objet
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en X
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en Y
  -- @param pFriction (OPTIONNEL) coefficient de friction du mouvement. Mettre à 0 pour ne pas appliquer
  -- @param pGravity (OPTIONNEL) coefficient de gravité. Mettre à 0 pour ne pas appliquer
  function self.initialize (pSensibilityX, pSensibilityY, pFriction, pGravity)
    debugFunctionEnter("settings.initialize")
    if (pSensibilityX == nil) then pSensibilityX = 1 end
    if (pSensibilityY == nil) then pSensibilityY = 1 end
    if (pFriction == nil) then pFriction = 5 end -- 5 pixels par seconde
    if (pGravity == nil) then pGravity = 20 end -- 20 pixels par seconde

    self.friction = pFriction
    self.gravity  = pGravity

    -- mapping clavier pour les touches globales à l'application
    self.appKeys = {
      nextLevel     = "kp+",
      loseLive      = "kp-",
      loseGame      = "end",
      winGame       = "home",
      quitGame      = "escape",
      debugSwitch   = "f9",
      moveWithKeys  = "f10",
      moveWithMouse = "f11",
      grabMouse     = "f12",
      pauseGame     = "p",
      startGame     = "r",
      previousMusic = "f1",
      nextMusic     = "f2"
    }

    -- mapping clavier pour les touches pouvant être modifiées par chaque joueur
    self.playerKeys = {
      action1      = "space",
      action2      = "lctrl",
      -- touches de déplacement
      moveUp       = "up",
      moveDown     = "down",
      moveLeft     = "left",
      moveRight    = "right",
      -- touches de déplacement alternatives
      moveUpAlt    = "z",
      moveDownAlt  = "s",
      moveLeftAlt  = "q",
      moveRightAlt = "d"
    }

    -- souris
    self.mouse = {
      sensibilityX = pSensibilityX,
      sensibilityY = pSensibilityY
    }
  end

  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("settings.destroy")
    self.clean()
    self = nil
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function self.clean ()
    debugFunctionEnter("settings.clean")
    -- pour le moment rien à faire
  end

  -- initialisation par défaut
  self.initialize()
  return self
end
