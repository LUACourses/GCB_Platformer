-- Entité représentant une map
-- Utilise une forme d'héritage simple: map qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent.
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/functions")
require("base/class")

-- type de map
MAP_TYPE_TILED = "map_type_tiled"
MAP_TYPE_NOT_TILED = "map_type_not_tiled"

-- dossier contenant les tuiles
MAP_TILES_FOLDER = "images/map"
-- dossier contenant les fichiers décrivant les niveaux
MAP_LEVELS_FOLDER = "levels"
-- dossier contenant le images du joueur
MAP_PLAYER_FOLDER = "images/player"
-- dossier contenant le images des pnj
MAP_PNJ_FOLDER = "images/pnj"
-- dossier contenant le images des décors
MAP_PROPS_FOLDER = "images/props"

-- type de tuile
TILE_SPECIAL_EMPTY = "0" -- vide
TILE_SPECIAL_PLAYER_START = "P" -- départ du joueur
TILE_SPECIAL_COIN = "c" -- une pièce
TILE_SPECIAL_DOOR = "D" -- la porte de sortie
TILE_SPECIAL_PNJ1 = "@" -- 1er type de PNJ

TILE_NO_SOLID = "tile_no_solid" -- non solide, traversable
TILE_SOLID = "tile_solid" -- solide, non traversable
TILE_JUMPTHROUGH = "tile_jumpthrough" -- solide, mais traversable avec un saut
TILE_LADDER = "tile_ladder" -- fait partie d'une échelle
TILE_INVISIBLE = "tile_invisible" -- invisible, juste pour utile pour détection d'objet
TILE_GO_LEFT = "tile_go_left" -- invisible, force un déplacement vers la gauche
TILE_GO_RIGHT = "tile_go_right" -- invisible, force un déplacement vers la droite
TILE_GO_TOP = "tile_go_top" -- invisible, force un déplacement vers le haut
TILE_GO_BOTTOM = "tile_go_bottom" -- invisible, force un déplacement vers le bas

-- taille par défaut des tuiles de la map
TILE_WIDTH = 16
TILE_HEIGHT = 16

---- Crée un pseudo objet de type map
-- @param pIndex (OPTIONNEL) numéro de la map (niveau de jeu) associé à la map.
-- @return un pseudo objet map
function newMap (pIndex)
  if (pIndex == nil) then pIndex = 1 end

  -- création d'un objet parent
  local self = class:new()

  -- attributs privés accessibles via les getter/setters
  -------------
  -- numéro du niveau
  local index = pIndex
  -- contenu de la map
  local grid = {}
  -- tuiles utilisées par la map
  local tiles = {}
  -- position de départ du joueur
  local playerStart = {}
  -- objets disponibles dans le niveau
  local objects = {}
  -- nombre de pièces disponibles dans le niveau (utile pour ouvrir la porte)
  local coinCount = 0
  -- les portes sont t-elles ouvertes ?
  local isDoorOpen = false
  -- type de map
  local type = MAP_TYPE_NOT_TILED
  -- nom associé à chaque map
  -- TODO : extraire le nom depuis le fichiers texte décrivant la map
  local names = {
    "The beginning",
    "Welcome in hell"
  }
  -- largeur des tuiles de la map
  local tileWidth = 0
  -- hauteur des tuiles de la map
  local tileHeight = 0

  --- initialize l'objet
  -- @param pIndex (OPTIONNEL) numéro de la map (niveau de jeu) associé à la map.
  function self.initialize (pIndex)
    debugFunctionEnter("map.initialize ", pIndex)
    if (pIndex == nil) then pIndex = 1 end

    index = pIndex
    playerStart = {
      col = 1,
      line = 1
    }
    objects = {}
    coinCount = 0
    isDoorOpen = false
    tiles ={}
    tileWidth = TILE_WIDTH
    tileHeight = TILE_HEIGHT
    type = MAP_TYPE_NOT_TILED
    if (love.filesystem.exists (MAP_LEVELS_FOLDER)) then
      if (love.filesystem.exists (MAP_TILES_FOLDER)) then
        tiles["0"] = {image = "", type = TILE_SPECIAL_EMPTY}
        tiles["P"] = {image = "", type = TILE_SPECIAL_PLAYER_START}
        tiles["1"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile1.png"), type = TILE_SOLID}
        tiles["2"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile2.png"), type = TILE_SOLID}
        tiles["3"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile3.png"), type = TILE_SOLID}
        tiles["4"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile4.png"), type = TILE_SOLID}
        tiles["5"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile5.png"), type = TILE_SOLID}
        tiles["="] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile=.png"), type = TILE_SOLID}
        tiles["["] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile[.png"), type = TILE_SOLID}
        tiles["]"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile].png"), type = TILE_SOLID}
        tiles["H"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tileH.png"), type = TILE_LADDER}
        tiles["#"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile#.png"), type = TILE_LADDER}
        tiles["g"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tileg.png"), type = TILE_JUMPTHROUGH}
        tiles[">"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile-arrow-right.png"), type = TILE_GO_RIGHT}
        tiles["<"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile-arrow-left.png"), type = TILE_GO_LEFT}
        tiles["^"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile-arrow-top.png"), type = TILE_GO_TOP}
        tiles["`"] = {image = love.graphics.newImage(MAP_TILES_FOLDER.."/tile-arrow-bottom.png"), type = TILE_GO_BOTTOM}

        tileWidth, tileHeight = tiles["1"].image:getDimensions() 
        type = MAP_TYPE_TILED
      else
        logMessage("map.initialize:le dossier '"..MAP_TILES_FOLDER.."'' contenant les images des tuiles n'a pas été trouvé")
        --TODO: taches spécifiques quand la map n'est pas "tiled"
      end
    else
      logMessage("map.initialize:le dossier '"..MAP_LEVELS_FOLDER.."'' contenant la définitions des niveaux n'a pas été trouvé")
      --TODO: taches spécifiques quand la map n'est pas "tiled"
    end
  end

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_index ()
    return index
  end
  function self.set_index (pValue)
    index = pValue
  end
  function self.get_grid ()
    return grid
  end
  function self.set_grid (pValue)
    grid = pValue
  end
  function self.get_tiles ()
    return tiles
  end
  function self.set_tiles (pValue)
    tiles = pValue
  end
  function self.get_playerStart ()
    return playerStart
  end
  function self.set_playerStart (pValue)
    playerStart = pValue
  end
  function self.get_objects ()
    return objects
  end
  function self.set_objects (pValue)
    objects = pValue
  end
  function self.get_coinCount ()
    return coinCount
  end
  function self.set_coinCount (pValue)
    coinCount = pValue
  end
  function self.get_names ()
    return names
  end
  function self.set_names (pValue)
    names = pValue
  end
  function self.get_isDoorOpen ()
    return isDoorOpen
  end
  function self.set_isDoorOpen (pValue)
    isDoorOpen = pValue
  end
  function self.get_type ()
    return type
  end
  function self.get_tileWidth ()
    return tileWidth
  end
  function self.set_tileWidth (pValue)
    tileWidth = pValue
  end
  function self.get_tileHeight ()
    return tileHeight
  end
  function self.set_tileHeight (pValue)
    tileHeight = pValue
  end
  -- fonctions spécifiques
  --------------------------------
  --- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("map.destroy")
    self.clean()
    self = nil
  end

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function self.clean ()
    debugFunctionEnter("map.clean")
    -- pour le moment rien à faire
  end

  --- Actualise l'objet (Override)
  -- @param pDt delta time
  function self.update (pDt)
    --debugFunctionEnter("map.update )", pDt) --ATTENTION  cet appel peut remplir le log
    -- pour le moment rien à faire
  end

  --- Dessine l'objet
  function self.draw ()
    --debugFunctionEnter("map.draw") --ATTENTION cet appel peut remplir le log
    if (type ~= MAP_TYPE_NOT_TILED) then
      assertEqualQuit(grid, nil, "map.draw:grid", true)
      assertEqualQuit(tiles, nil, "map.draw:tiles", true)

      local l, c, char, drawX, drawY
      for l = 1, #grid do
        for c = 1, #grid[l] do
          char = string.sub(grid[l], c, c)
          if (char ~= nil) then
            if (tiles ~= nil and tiles[char] ~= nil and not self.isInvisible(char) and tiles[char].image ~="") then
              drawX, drawY = self.mapToPixel(c, l)
              HUD.drawImage(tiles[char].image, drawX, drawY)
            end
          end
        end
      end
    end --if (type ~= MAP_TYPE_NOT_TILED) then
  end

  --- Convertit des coordonnées de map vers des coordonnées en pixel
  function self.mapToPixel (pCol, pLine)
    return (pCol - 1) * tileWidth, (pLine - 1) * tileHeight
  end

  --- Convertit des coordonnées en pixel vers des coordonnées de map
  function self.pixelToMap (pX, pY)
    return math.floor(pX / tileWidth) + 1, math.floor(pY / tileHeight) + 1
  end

  -- Charge le fichier contenant la map
  function self.load ()
    debugFunctionEnter("map.load")
    if (type ~= MAP_TYPE_NOT_TILED) then

      local filename, line
      filename = MAP_LEVELS_FOLDER.."/level"..tostring(index)..".txt"

      if (love.filesystem.exists(filename)) then
        for line in love.filesystem.lines(filename) do
          grid[#grid + 1] = line
        end

        -- si on a déjà chargé un niveau, efface tous les sprites sauf le joueur
        if (player ~= nil) then player.resetAll() end

        -- Analyse de la map
        local l, c, char, objet, key
        for l = 1, #grid do
          for c = 1, #grid[1] do
            key = l.."##"..c
            char = string.sub(grid[l],c,c)
            if (char == TILE_SPECIAL_PLAYER_START) then
              -- point de départ du joueur
              playerStart.col = c
              playerStart.line = l
            elseif (char == TILE_SPECIAL_COIN) then
              -- une piece
              object = self.createCoin(c, l)
              objects[key] = {type = char, content = object, col = c, line = l}
              coinCount = coinCount + 1
            elseif (char == TILE_SPECIAL_DOOR) then
              -- une porte
              object = self.createDoor(c, l)
              objects[key] = {type = char, content = object, col = c, line = l}
            elseif (char == TILE_SPECIAL_PNJ1) then
              -- un pnj goomba 
              object = self.createPNJ1(c, l)
              objects[key] = {type = char, content = object, col = c, line = l}
            end
          end
        end
      else
        logMessage("map.load:le fichier "..filename.." n'a pas été trouvé")
        -- plus de niveaux à charger, la partie est gagnée
        winGame()
      end --if (love.filesystem.exists(filename)) then
    end --if (type ~= MAP_TYPE_NOT_TILED) then
  end

  --- Retourne l'index de la tuile de la map située au coordonnées en pixels données
  -- @param pX position en X en pixel
  -- @param pY position en Y en pixel
  -- @return Index de la tuile
  function self.getTileAt (pX, pY)
    --debugFunctionEnter("map.getTileAt )", pX, pY) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "map.getTileAt:grid", true)
    local l, c
    c,l = self.pixelToMap(pX, pY)
    if (l > 0 and l <= #grid and c > 0 and c <= #grid[1]) then
      return string.sub(grid[l], c, c)
    end
    return nil
  end

  --- Indique si la une tuile est solide ou pas (IE. elle bloque le joueur)
  -- @param pTileId Index de la tuile
  -- @return true ou false
  function self.isSolid (pTileId)
    --debugFunctionEnter("map.isSolid )", pTileId) --ATTENTION cet appel peut remplir le log
    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_SOLID) then return true end
    return false
  end

  --- Indique si la une tuile peut être traversée par un saut ou pas
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function self.isJumpThrough (pTileId)
    --debugFunctionEnter("map.isJumpThrough )", pTileId) --ATTENTION cet appel peut remplir le log
    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_JUMPTHROUGH) then return true end
    return false
  end

  --- Indique si la une tuile fait partie d'une échelle
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function self.isLadder (pTileId)
    --debugFunctionEnter("map.isLadder )", pTileId) --ATTENTION cet appel peut remplir le log
    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_LADDER) then return true end
    return false
  end

  --- Indique si la une tuile est invisible pour le joueur
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function self.isInvisible (pTileId)
    --debugFunctionEnter("map.isInvisible )", pTileId) --ATTENTION cet appel peut remplir le log
    if (DEBUG_SHOW_MAP_INVISIBLE) then return false end
    if (tiles ~= nil and tiles[pTileId] ~= nil and (
        tiles[pTileId].type == TILE_GO_LEFT
        or tiles[pTileId].type == TILE_GO_RIGHT 
        or tiles[pTileId].type == TILE_GO_TOP
        or tiles[pTileId].type == TILE_GO_BOTTOM
        or pTileId == TILE_SPECIAL_PLAYER_START 
        or pTileId == TILE_SPECIAL_EMPTY 
        or pTileId == TILE_SPECIAL_PNJ1 
      )
    ) then 
      return true 
    end
    return false
  end

  --- Crée un pseudo objet de type coin
  -- @param pCol: colonne où positionner l'objet
  -- @param pLine: ligne où positionner l'objet
  -- @return un pseudo objet coin
  function self.createCoin (pCol, pLine)
    --debugFunctionEnter("map.createCoin )", pCol, pLine)
    local sprite
    local posX, posY = self.mapToPixel(pCol, pLine)

    sprite = newSprite()
    sprite.initialize(SPRITE_TYPE_PROP__COIN, posX, posY, tileWidth, tileHeight, MAP_PROPS_FOLDER.."/coin/coin1.png")

    -- ajoute les animations
    if (love.filesystem.isFile(MAP_PROPS_FOLDER.."/coin/coin1.png")) then sprite.addAnimation(MAP_PROPS_FOLDER.."/coin", SPRITE_ANIM_IDLE, {"coin1", "coin2", "coin3", "coin4"}) end

    sprite.playAnimation(SPRITE_ANIM_IDLE)
    return sprite
  end

  --- Crée un pseudo objet de type door
  -- @param pCol: colonne où positionner l'objet
  -- @param pLine: ligne où positionner l'objet
  -- @return un pseudo objet door
  function self.createDoor (pCol, pLine)
    --debugFunctionEnter("map.createDoor )", pCol, pLine)
    local sprite
    local posX, posY = self.mapToPixel(pCol, pLine)

    sprite = newSprite()
    sprite.initialize(SPRITE_TYPE_ELT__DOOR, posX, posY, tileWidth, tileHeight, MAP_PROPS_FOLDER.."/door/door-close.png")

    -- ajoute les animations
    if (love.filesystem.isFile(MAP_PROPS_FOLDER.."/door/door-close.png")) then sprite.addAnimation(MAP_PROPS_FOLDER.."/door", SPRITE_ANIM_CLOSE, {"door-close"}) end
    if (love.filesystem.isFile(MAP_PROPS_FOLDER.."/door/door-open.png")) then sprite.addAnimation(MAP_PROPS_FOLDER.."/door", SPRITE_ANIM_OPEN, {"door-open"}) end

    sprite.playAnimation(SPRITE_ANIM_CLOSE)
    return sprite
  end

  --- Crée un pseudo objet de type PNJ1
  -- @param pCol: colonne où positionner l'objet
  -- @param pLine: ligne où positionner l'objet
  -- @return un pseudo objet PNJ1
  function self.createPNJ1 (pCol, pLine)
    --debugFunctionEnter("map.createPNJ )", pCol, pLine)
    local sprite
    local posX, posY = self.mapToPixel(pCol, pLine)

    sprite = newSprite()
    sprite.initialize(SPRITE_TYPE_PNJ__BLOB, posX, posY, tileWidth, tileHeight, MAP_PNJ_FOLDER.."/pnj1/walk1.png",0 , 100, 0, 0, 100, false)

    -- ajoute les animations
    if (love.filesystem.isFile(MAP_PNJ_FOLDER.."/pnj1/walk1.png")) then sprite.addAnimation(MAP_PNJ_FOLDER.."/pnj1", SPRITE_ANIM_WALK, {"walk1", "walk2", "walk3", "walk4", "walk5", "walk6"}) end

    sprite.playAnimation(SPRITE_ANIM_WALK)
    direction = SPRITE_MOVE_RIGHT
    return sprite
  end
  
  --- Ajoute une valeur au compteur de pièces
  -- @param pValue Valeur à ajouter
  function self.add_coinCount (pValue)
    coinCount = coinCount + pValue
  end

  -- initialisation par défaut
  self.initialize(pIndex)
  return self
end
