-- Écran de Pause
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées

--- Actualise l'écran de pause
-- @param pDt delta time
function updatePauseScreen (pDt)
  --debugFunctionEnter("updatePauseScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end

--- Affiche l'écran de pause
function drawPauseScreen ()
  --debugFunctionEnter("drawPauseScreen") --ATTENTION cet appel peut remplir le log
  assertEqualQuit(viewport, nil, "drawStartScreen:viewport", true)
  assertEqualQuit(HUD, nil, "drawStartScreen:HUD", true)

  local color, content
  color = {255, 160, 0, 255} --couleur orange sans transparence
  content = "PAUSE"
  HUD.displayTextScale(content, HUD.get_fontMenuTitle(), POS_CENTER, 0, 0, color)
  content = 'Appuyer sur "'..settings.appKeys.pauseGame..'" pour reprendre'
  HUD.displayTextScale(content, HUD.get_fontMenuContent(), POS_CENTER, 0, 50)
  HUD.set_lastDrawScreen(appState.get_currentState())
end

--- Gestion du "keypressed" sur l'écran de pause
-- @param dynamiques
function keypressedPauseScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedPauseScreen", pKey, pScancode, pIsrepeat)
  -- Sortir l'application du mode pause
  if (pKey == settings.appKeys.pauseGame) then resumeGame() end
end

--- Gestion du "mousepressed" sur l'écran de pause
-- @param dynamiques
function mousepressedPauseScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedPauseScreen")
  -- rien à faire pour le moment
end

--- Gestion du "mousemoved" sur l'écran de pause
-- @param dynamiques
function mousemovedPauseScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedPauseScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end