-- Écran de jeu
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées

--- Actualise l'écran de jeu
-- @param pDt delta time
function updatePlayScreen (pDt)
  --debugFunctionEnter("updatePlayScreen") --ATTENTION cet appel peut remplir le log
  -- déplacements et collisions du joueur
  
  -- gère les déplacements
  local indexSprite, sprite
  for indexSprite = #spriteList, 1, -1 do
    sprite = spriteList[indexSprite]
    debugVar = sprite.tostring() -- Cette variable globale permet de voir les infos du sprite dans la pile d'execution en debug 
    -- sprite.CCD(pDt, pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax)
    sprite.CCD(pDt) -- fait le update, le collide et le move
    
    -- on actualise l'index du sprite au cas ou il y aurait eu des suppressions
    sprite.tableIndex = indexSprite 
  end
  
  debugInfos = "joueur: X= "..math.floor(player.get_x()).." Y= "..math.floor(player.get_y()).." W= "..math.floor(player.get_width()).." H= "..math.floor(player.get_height())
  debugInfos = debugInfos.."\nVelocityX = "..math.floor(player.get_vX()).."\nVelocityY= "..math.floor(player.get_vY()).."\nSpeed= "..math.floor(player.get_speed())
end

--- Affiche l'écran de jeu
function drawPlayScreen ()
  --debugFunctionEnter("drawPlayScreen") --ATTENTION cet appel peut remplir le log
  assertEqualQuit(HUD, nil, "drawStartScreen:HUD", true)
  assertEqualQuit(viewport, nil, "drawStartScreen:viewport", true)
  assertEqualQuit(settings, nil, "drawPlayScreen:settings", true)
  assertEqualQuit(appState, nil, "drawPlayScreen:appState", true)
  assertEqualQuit(player, nil, "drawPlayScreen:player", true)

  -- affiche la carte
  map.draw()

  -- affiche tous les sprites, y compris le joueur

  -- BLOC SPR_DRAWALL
  -- Dessine tous les sprites de la liste
  if (spriteList ~= nil) then
    local indexSprite, sprite
    for indexSprite = #spriteList, 1, -1 do
      sprite = spriteList[indexSprite]
      sprite.draw()
    end
  end
  -- FIN BLOC SPR_DRAWALL

  -- BLOC HUD_DISP
  local color, content
  local charWidth = HUD.get_fontGUI():getWidth("A")
  local charHeight = HUD.get_fontGUI():getHeight("A")
  local Xdraw = viewport.get_width()
  local Ydraw = viewport.get_height()

  -- affiche le nombre de vie du joueur
  -- s'il est négatif, on affiche rien
  if (player.get_livesLeft() >= 0) then
    local imgPath = player.get_livesImagePath()

    if (imgPath ~= "") then
      -- si le joueur a une image, on affiche l'image pour les vies restante
      love.graphics.setColor(255, 255, 255, 255) -- pour que les images soit visibles

      local i, livesImgWidth, livesImgHeight, livesImg
      livesImg = love.graphics.newImage(imgPath)
      livesImgWidth,livesImgHeight = livesImg:getDimensions()
      Xdraw = viewport.Xmax - (livesImgWidth + 5) * player.get_livesStart() - 5
      Ydraw = 0
      for i = 0, player.get_livesLeft() - 1 do
        HUD.drawImageScale(livesImg, Xdraw + (livesImgWidth + 5) * i, Ydraw, 0, 1, 1, 0, 0, 0, 5)
      end
    else
      -- sinon affiche un texte
      HUD.displayTextScale("Vies "..player.get_livesLeft(), HUD.get_fontGUI(), POS_TOP_RIGHT, -10, 5, {viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 255})
    end
  end

  -- affiche le score du joueur
  -- s'il est négatif, on affiche rien
  if (player.get_score() >= 0) then
    Ydraw = 5
    HUD.displayTextScale("Score "..player.get_score(), HUD.get_fontGUI(), POS_TOP_LEFT, 10, Ydraw, {255, 160, 0, 255}) --couleur orange sans transparence
  end

  -- affiche le niveau du joueur
  -- s'il est négatif, on affiche rien
  if (player.get_level() >= 0) then
    local rectWidth = 12
    local rectHeight = 25
    content = "Niveau "
    HUD.displayTextScale(content, HUD.get_fontGUI(), POS_BOTTOM_LEFT, 5, -1, {255, 160, 0, 255}) --couleur orange sans transparence

    Xdraw = HUD.get_fontGUI():getWidth(content) * SCREEN_SCALE_X
    Ydraw = (viewport.Ymax - viewport.Ymin)
    for i = 0, appState.get_levelToWin() - 1 do
      if (i < player.get_level()) then
        love.graphics.setColor(255, 160, 0, 255) --couleur orange sans transparence
      else
        love.graphics.setColor(viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 128)
      end
      HUD.rectangleScale("fill", Xdraw + (i * rectWidth), Ydraw, rectWidth - 1, rectHeight, 0 , -charHeight)
    end
  end

  -- affiche le nom du niveau de la map
  content = "Level "..map.get_index()
  local name = map.get_names()[map.get_index()]
  if ( name~= nil) then content = content ..": "..name end
  HUD.displayTextScale(content, HUD.get_fontGUI(), POS_TOP_CENTER, 0, 5, {255, 255, 255, 255}) --couleur blanc sans transparence

  -- affichage des infos de debug (en bas de l'écran)
  if (DEBUG_MODE >= 2 and debugInfos ~= "") then
    HUD.displayTextScale(debugInfos, HUD.get_fontDebug(), POS_BOTTOM_RIGHT, -5, -50, {0, 200, 0, 128}, 500, nil) -- vert foncé 50%
  end
  love.graphics.setColor(255, 255, 255, 255) --RAZ des couleur

  HUD.set_lastDrawScreen(appState.get_currentState())
  -- FIN BLOC HUD_DISP
end

--- Gestion du "keypressed" sur l'écran de jeu
-- @param dynamiques
function keypressedPlayScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedPlayScreen", pKey, pScancode, pIsrepeat)
  if (DEBUG_MODE > 0) then
    -- en mode debug , il existe une touche pour tuer la partie
    if (pKey == settings.appKeys.nextLevel) then nextLevel();return end
    if (pKey == settings.appKeys.loseLife) then loseLife();return end
    if (pKey == settings.appKeys.loseGame) then loseGame();return end
    if (pKey == settings.appKeys.winGame) then winGame();return end
    if (pKey == settings.appKeys.debugSwitch) then 
      -- permet de déclancher des évènement de débug en live
      debugBreakpoints = not debugBreakpoints
    end

  end

  if (pKey == settings.appKeys.pauseGame) then pauseGame() end

  -- NOTE: le deplacement du joueur avec les touches se fait dans player.move()
  player.keypressed(pKey, pScancode, pIsrepeat)
  -- oblige le joueur à relacher des touches prédéfinies après une action donnée
  --if (player.get_keysToRelease() == nil or player.get_keyToRelease(pKey) == nil or player.get_keyToRelease(pKey) == false) then
  --  player.keypressed(pKey, pScancode, pIsrepeat)
  --end
end

--- Gestion du "mousepressed" sur l'écran de jeu
-- @param dynamiques
function mousepressedPlayScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedPlayScreen")
  player.mousepressed(pX, pY, pButton, pIstouch)
end

--- Gestion du "mousemoved" sur l'écran de jeu
-- @param dynamiques
function mousemovedPlayScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedPlayScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  player.mousemoved(pX, pY, pDX, pDY, pIstouch)
end