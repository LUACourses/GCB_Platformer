-- Écran de fin
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées

--- Actualise l'écran de fin
-- @param pDt delta time
function updateEndScreen (pDt)
  --debugFunctionEnter("updateEndScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
  player.resetAll()
end

--- Affiche l'écran de fin
function drawEndScreen ()
  --debugFunctionEnter("drawEndScreen") --ATTENTION cet appel peut remplir le log
  assertEqualQuit(viewport, nil, "drawStartScreen:viewport", true)
  assertEqualQuit(HUD, nil, "drawStartScreen:HUD", true)
  local color, content
  if (player.get_hasWon() == true) then
    content = "Bravo,partie gagnee !"
    color = {0, 200, 0, 255} --couleur vert sans transparence
  else
    content = "GAME OVER :o("
    color = {200, 0, 0, 255} --couleur rouge sans transparence
  end
  local textWidth = HUD.get_fontMenuTitle():getWidth(content)
  HUD.animText(content, HUD.get_fontMenuTitle(), TEXT_ANIM_SCROLL_HORZ, 0, 0, nil, nil, {color}, 1, 200, - textWidth, nil)

  HUD.displayTextScale('Appuyer sur "'..settings.appKeys.quitGame ..'" pour quitter le jeu', HUD.get_fontMenuContent(), POS_CENTER, 0, 50, {255, 160, 0, 255}) --couleur orange sans transparence
  HUD.displayTextScale('Appuyer sur "'..settings.appKeys.startGame..'" pour rejouer', HUD.get_fontMenuContent(), POS_CENTER, 0 , 80)

  HUD.displayTextScale("Votre score est de "..player.get_score(), HUD.get_fontMenuContent(), POS_CENTER, 0, 110)
  HUD.set_lastDrawScreen(appState.get_currentState())
end

--- Gestion du "keypressed" sur l'écran de fin
-- @param dynamiques
function keypressedEndScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedEndScreen", pKey, pScancode, pIsrepeat)
  if (pKey == settings.appKeys.endGame) then quitGame();return end
  if (pKey == settings.appKeys.startGame) then startGame();return end
end

--- Gestion du "mousepressed" sur l'écran de fin
-- @param dynamiques
function mousepressedEndScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedEndScreen")
  -- rien à faire pour le moment
end

--- Gestion du "mousemoved" sur l'écran de fin
-- @param dynamiques
function mousemovedEndScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedEndScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end