-- Écran de Démarrage
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées

--- Actualise l'écran (menu) de départ
-- @param pDt delta time
function updateStartScreen (pDt)
  --debugFunctionEnter("updateStartScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end

--- Affiche l'écran (menu) de départ
function drawStartScreen ()
  --debugFunctionEnter("drawStartScreen") --ATTENTION cet appel peut remplir le log
  assertEqualQuit(viewport, nil, "drawStartScreen:viewport", true)
  assertEqualQuit(HUD, nil, "drawStartScreen:HUD", true)

  local color, content
  color = {255, 160, 0, 255} --couleur vert sans transparence

  HUD.animText(love.window.getTitle(), HUD.get_fontMenuTitle(), TEXT_ANIM_SINUS_CENTER, 0, -50, nil, nil, {color}, 1, 70)
  content = "Appuyer sur une touche ou cliquer pour lancer le jeu"
  HUD.displayTextScale(content, HUD.get_fontMenuContent(), POS_CENTER, 0, 50, color)
  HUD.set_lastDrawScreen(appState.get_currentState())
end

--- Gestion du "keypressed" sur l'écran (menu) de départ
-- @param dynamiques
function keypressedStartScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedStartScreen", pKey, pScancode, pIsrepeat)
  if (pKey ~= nil) then
    startGame()
  end
end

--- Gestion du "mousepressed" sur l'écran (menu) de départ
-- @param dynamiques
function mousepressedStartScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedStartScreen")
  startGame()
end

--- Gestion du "mousemoved" sur l'écran (menu) de départ
-- @param dynamiques
function mousemovedStartScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedStartScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end
