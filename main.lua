-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf("no")

-- requires obligatoires pour le framework
-- Attention, l'ordre est important
require("app/settings")
require("app/appState")
require("base/constants")
require("base/stack")
require("base/functions")
require("base/musicManager")
require("base/viewport")
require("base/player")
require("base/HUD")

-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées pour les différents écrans
require("screens/start")
require("screens/play")
require("screens/pause")
require("screens/end")

-- requires spécifiques à l'application
require("app/functions")
require("app/map")

-- Filtre pour adoucir les contours des images quand elles sont redimentionnées
-- NOTE : à désactiver pour du pixel art ou du style old school ou des facteur d'échelle important
love.graphics.setDefaultFilter("nearest")

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

-- variables diverses
-- texte de debug affiché à l'écran par le HUD
debugInfos = ""
-- variable globale commune à tous les modules utilisé pour du debug
debugVar = ""
-- permet de définir des zones de code spécifiques pour activer des breakpoints
debugBreakpoints = false
-- différents timers pour gérer les animations utilisées dans l'application
timersForAnimation = newStack()

--- Love.load pour initialiser l'application
function love.load ()
  --debugInfos = "Programme démarré...\n"

  -- positionne la fenêtre sur l'écran défini pour le debug
  if (DEBUG_MODE > 0) then love.window.setPosition(DEBUG_WINDOWS_X, DEBUG_WINDOWS_Y, DEBUG_DISPLAY_NUM) end

  -- Initialisation du random avec un temps en ms
  math.randomseed(love.timer.getTime())

  -- La répétition de l'événement love.keypressed est activés lorsqu'une touche est maintenue enfoncée.
  love.keyboard.setKeyRepeat(true)

  -- Active le mode relatif: le curseur est caché et ne se déplace pas lorsque la souris est activée,
  -- mais les événements relatifs du mouvement de la souris sont toujours générés via love.mousemoved
  love.mouse.setRelativeMode(true)

  initializeGame()

  if (appState.get_currentState() == SCREEN_START) then startMenu() else startGame() end
end

--- Love.update est utilisé pour gérer l'état de l'application frame-to-frame
-- @param pDt delta time
function love.update (pDt)

  --player.update(pDt) --fait dans le BLOC SPR_DEP de updatePlayScreen

  HUD.update(pDt)

  --Actualise le contenu de l'écran
  if (appState.get_currentState() == SCREEN_START) then
    ---------------
    -- écran de démarrage
    ---------------
    updateStartScreen(pDt)
  elseif (appState.get_currentState() == SCREEN_PAUSE) then
    ---------------
    -- écran de pause
    ---------------
    updatePauseScreen(pDt)
  elseif (appState.get_currentState() == SCREEN_END) then
    ---------------
    -- écran de fin
    ---------------
    updateEndScreen(pDt)
  elseif (appState.get_currentState() == SCREEN_PLAY) then
    ---------------
    -- écran de jeu
    ---------------
    updatePlayScreen(pDt)
  end

  --Actualise les musiques
  musicManager.update(pDt)
end

--- Love.draw est utilisé pour afficher l'état de l'application sur l'écran.
function love.draw ()

  HUD.draw()--normalement cette fonction ne fait pas grand chose car les écrans sont dessinés dans les fonctions drawXXXScreen

  -- applique l'échelle pour l'affichage
  if (SCREEN_SCALE_X  ~= 1 or SCREEN_SCALE_Y ~= 1) then love.graphics.scale(SCREEN_SCALE_X, SCREEN_SCALE_Y) end

  -- si ce n'a pas déjà été fait,on dessine le viewport
  if (not viewport.firstDrawDone) then viewport.draw() end

  -- affiche l'écran de jeu
  if (appState.get_currentState() == SCREEN_START) then
    ---------------
    -- écran de démarrage
    ---------------
    drawStartScreen()
  elseif (appState.get_currentState() == SCREEN_PAUSE) then
    ---------------
    -- écran de pause
    ---------------
    drawPauseScreen()
  elseif (appState.get_currentState() == SCREEN_END) then
    ---------------
    -- écran de fin
    ---------------
    drawEndScreen()
  elseif (appState.get_currentState() == SCREEN_PLAY) then
    ---------------
    -- écran de jeu
    ---------------

    -- Affiche le joueur
    --player.draw() --fait dans player.drawAll() de drawPlayScreen
    drawPlayScreen()
  else
    -- écran invalide (erreur dans le processus, ou mauvaise utilisation des constantes SCREEN_xxx)
    ---------------
    HUD.drawBadScreen()
  end
end

--- intercepte l'appui des touches du clavier
function love.keypressed (pKey, pScancode, pIsrepeat)
  --debugMessage("touche clavier appuyée:", pKey, pScancode, pIsrepeat)

  -- évènements indépendants de l'écran en cours d'affichage
  ------------
  -- Quitter l'application
  if (pKey == settings.appKeys.quitGame) then quitGame();return end
  -- Capturer ou non la souris dans la fenêtre
  if (pKey == settings.appKeys.grabMouse) then
    local state = not love.mouse.isGrabbed()
    love.mouse.setGrabbed(state)
    debugMessage("mouse.setGrabbed:", state)
  end

  -- Afficher les infos de debug dans la console
  if (pKey == settings.appKeys.debugInfos) then
    debugMessage (debugInfos)
    debugMessage ("INFO PLAYER:", player.tostring())
    DEBUG_BP_ACTIVE = not DEBUG_BP_ACTIVE
  end

  -- Jouer la piste musicale suivante
  if (pKey == settings.appKeys.nextMusic) then musicManager.playNext();return end
  -- Jouer la piste musicale précédente
  if (pKey == settings.appKeys.previousMusic) then musicManager.playPrevious();return end

  -- évènements liés à l'écran en cours d'affichage
  ------------
  if (appState.get_currentState() == SCREEN_START) then
    -- écran de démarrage
    keypressedStartScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.get_currentState() == SCREEN_PAUSE) then
    -- écran de pause
    keypressedPauseScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.get_currentState() == SCREEN_END) then
    -- écran de fin
    keypressedEndScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.get_currentState() == SCREEN_PLAY) then
    -- écran de jeu
    keypressedPlayScreen(pKey, pScancode, pIsrepeat)
  end
end

--- intercepte le relâchement des touches du clavier
function love.keyreleased (pKey, pScancode)
  -- si une touche figure dans la liste des touche à relacher, on mémorise que cette touche a été relâchée
  if (player.get_keysToRelease() ~= nil and player.get_keyToRelease(pKey) ~= nil ) then player.set_keyToRelease(pKey, false) end
end

--- intercepte le mouvement de la souris
function love.mousemoved (pX, pY, pDX, pDY, pIstouch)
  --debugMessage("déplacement de la souris:", pX, pY, pDX, pDY, pIstouch)  --ATTENTION cet appel peut remplir le log

  -- évènements indépendants de l'écran en cours d'affichage
  ------------

  -- évènements liés à l'écran en cours d'affichage
  ------------
  if (appState.get_currentState() == SCREEN_START) then
    -- écran de démarrage
    mousemovedStartScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.get_currentState() == SCREEN_PAUSE) then
    -- écran de pause
    mousemovedPauseScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.get_currentState() == SCREEN_END) then
    -- écran de fin
    mousemovedEndScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.get_currentState() == SCREEN_PLAY) then
    -- écran de jeu
    mousemovedPlayScreen(pX, pY, pDX, pDY, pIstouch)
  end
end

--- intercepte le clic de souris
function love.mousepressed (pX, pY, pButton, pIstouch)
  --debugMessage("boutons de la souris appuyé:",pX, pY, pButton, pIstouch)

  -- évènements indépendants de l'écran en cours d'affichage
  ------------

  -- évènements liés à l'écran en cours d'affichage
  ------------
  if (appState.get_currentState() == SCREEN_START) then
    -- écran de démarrage
    mousepressedStartScreen(pX, pY, pButton, pIstouch)
  elseif (appState.get_currentState() == SCREEN_PAUSE) then
    -- écran de pause
    mousepressedPauseScreen(pX, pY, pButton, pIstouch)
  elseif (appState.get_currentState() == SCREEN_END) then
    -- écran de fin
    mousepressedEndScreen(pX, pY, pButton, pIstouch)
  elseif (appState.get_currentState() == SCREEN_PLAY) then
    -- écran de jeu
    mousepressedPlayScreen(pX, pY, pButton, pIstouch)
  end
end

--- Initialiser l'application
function initializeGame ()
  -- creation des entités
  -- Entité représentant les paramètres de l'application
  settings = newSettings()
  -- Entité représentant l'état de l'application à un instant donné
  appState = newAppState()
  -- Entité représentant la zone de jeu
  viewport = newViewport()
   -- Entité représentant le music manager
  musicManager = newMusicManager()
   -- Entité représentant l'affichage
  HUD = newHUD()

  -- Entité représentant la carte de jeu
  map = newMap(pLevel)

  -- Entité représentant le joueur
  player = newPlayer()

  -- charge les musiques non gérées par le musicManager (menu, écran de fin)
  musicMenu = love.audio.newSource("musics/menu_Cool.mp3","stream")
  musicMenu:setLooping(true)
  musicMenu:setVolume(1)
  musicEnd = love.audio.newSource("musics/end_NodensField.mp3","stream")
  musicEnd:setLooping(true)
  musicEnd:setVolume(0.5)
  musicWin = love.audio.newSource("musics/win_Someday.mp3","stream")
  musicWin:setLooping(true)
  musicWin:setVolume(0.1)

  -- charge les musiques gérées par le musicManager
  musicManager.add("musics/mm_Platformer2.mp3","stream")
  musicManager.add("musics/mm_DefenseLine.mp3","stream")

  -- charge les sons du jeu
  sndNextLevel = love.audio.newSource("sounds/nextLevel.mp3","static")
  sndLoseLife = love.audio.newSource("sounds/loseLife.mp3","static")
end

--- Menu de démarrage
function startMenu ()
  debugMessage("Menu de démarrage")
  appState.initialize(3) -- niveau 3 pour gagner
  appState.set_hasStarted(true)
  appState.set_currentState(SCREEN_START)

  musicManager.stop()
  musicEnd:stop()
  musicWin:stop()
  musicMenu:play()
end

--- Lancer l'application
function startGame ()
  debugMessage("Jeu démarré")
  if (not appState.get_hasStarted()) then
    -- cas ou on n'est pas passé par le startMenu
    appState.initialize(3) -- niveau 3 pour gagner
  end
  
  local startLevel = 1
  
  -- Entité représentant la carte de jeu
  map = newMap(startLevel)
  -- charge la carte du niveau
  map.load()
  
  if (map.get_type() ~= MAP_TYPE_NOT_TILED) then
    player.initialize(
      0, -- pX
      0, -- pY
      nil, -- pWidth (calculé automatiquement en fonction de la map)
      nil, -- pHeight (calculé automatiquement en fonction de la map)
      "images/player/idle1.png", -- pImage
      -600, -- pVelocityJump
      600, -- pStartSpeed
      200, -- pFriction
      1500, -- pStartGravity
      200, -- pVelocityMax. NOTE: en augmentant ce paramètre on diminue la limite de vélocité et on augmente l'inertie du mouvement (par exemple on rend le sol moins adhérent)
      1, -- pWeight. NOTE: en augmentant ce paramètre on augmente la vitesse de chute et diminue la hauteur des sauts
      nil, -- pLiveImagePath
      startLevel, -- pLevel
      3, -- pLivesStart
      0, -- pScore
      true, -- pMoveWithVelocity
      false, -- pMoveWithMouse
      true, -- pMoveWithKeys
      nil -- pColor
    )
    -- test only player.set_gravityModularity(200)

    -- Ajoute les animations du joueur (si les images nécessaires sont présentes)
    if (love.filesystem.isFile(MAP_PLAYER_FOLDER.."/idle1.png")) then player.addAnimation(MAP_PLAYER_FOLDER, SPRITE_ANIM_IDLE, {"idle1", "idle2", "idle3", "idle4"}) end
    if (love.filesystem.isFile(MAP_PLAYER_FOLDER.."/run1.png")) then player.addAnimation(MAP_PLAYER_FOLDER, SPRITE_ANIM_RUN, {"run1", "run2", "run3", "run4", "run5", "run6", "run7", "run8", "run9", "run10"}) end
    if (love.filesystem.isFile(MAP_PLAYER_FOLDER.."/climb1.png")) then player.addAnimation(MAP_PLAYER_FOLDER, SPRITE_ANIM_CLIMB, {"climb1", "climb2"}) end
    if (love.filesystem.isFile(MAP_PLAYER_FOLDER.."/climb1.png")) then player.addAnimation(MAP_PLAYER_FOLDER, SPRITE_ANIM_CLIMB_IDLE, {"climb1"}) end

    player.playAnimation(SPRITE_ANIM_IDLE)
  else
    player.initialize(
      0, -- pX
      0, -- pY
      32, -- pWidth
      32, -- pHeight
      nil, -- pImage
      0, -- pVelocityJump
      500, -- pStartSpeed
      50, -- pFriction
      100, -- pStartGravity
      300, -- pVelocityMax
      2, -- pWeight
      MAP_PLAYER_FOLDER.."/player_life.png", -- pLiveImagePath
      startLevel, -- pLevel
      3, -- pLivesStart
      0, -- pScore
      true, -- pMoveWithVelocity
      true, -- pMoveWithMouse
      true, -- pMoveWithKeys
      {255, 46, 0, 255}) -- pColor
   end

  appState.set_currentState(SCREEN_PLAY)

  musicMenu:stop()
  musicEnd:stop()
  musicWin:stop()

  musicManager.play(startLevel)

  -- place le joueur dans la carte
  if (map.get_type() == MAP_TYPE_NOT_TILED) then
    player.spawn()
  else
    --player.spawnToMap(map.get_playerStart().col, map.get_playerStart().line)
    player.spawnToMap()
  end

end

--- Mettre l'application en pause
function pauseGame()
  debugMessage("Pause")
  musicMenu:play()
  musicManager.pause()
  appState.set_currentState(SCREEN_PAUSE)
end

--- Sortir l'application du mode pause
function resumeGame()
  debugMessage("Resume")
  musicMenu:stop()
  musicManager.resume()
  appState.set_currentState(SCREEN_PLAY)
end

--- le joueur perd une "vie"
function loseLife ()
  logMessage("Vie perdue")
  if (player.get_livesLeft() <= 1) then
    player.clean() -- juste au cas ou il y aurait une animation dans la destruction du joueur
    loseGame()
  else
    sndLoseLife:play()
    player.loseLife(1)
    -- place le joueur dans la carte
    if (map.get_type() == MAP_TYPE_NOT_TILED) then
      player.spawn()
    else
      --player.spawnToMap(map.get_playerStart().col, map.get_playerStart().line)
      player.spawnToMap()
    end
  end
end

--- le joueur passe au niveau suivant
-- @param pNewLevel (OPTIONNEL) valeur du nouveau niveau. Si absent, le niveau courant sera incrémenté de 1
function nextLevel (pNewLevel)
  if (appState.get_currentState() == SCREEN_NEXT) then
    return
  else
    appState.set_currentState(SCREEN_NEXT)
  end
  if (pNewLevel == nil) then
    logMessage("Niveau suivant")
    player.addLevel(1)
  else
    logMessage("Niveau atteint:", pNewLevel)
    player.set_level(pNewLevel)
  end

  if (player.get_level() >= appState.get_levelToWin()) then
    -- l'application est terminée lorsque le niveau max est atteint
    winGame()
  else
    sndNextLevel:play()
    musicManager.playNext()

    -- Entité représentant la carte de jeu
    map = newMap(player.get_level())
    -- charge la carte du niveau
    map.load()

    -- place le joueur dans la carte
    if (map.get_type() == MAP_TYPE_NOT_TILED) then
      player.spawn()
    else
      --player.spawnToMap(map.get_playerStart().col, map.get_playerStart().line)
      player.spawnToMap()
    end

    appState.set_currentState(SCREEN_PLAY)
  end
end

--- le joueur perd la partie
function loseGame ()
  logMessage("Partie perdue")
  sndLoseLife:play()
  player.set_hasWon(false)
  musicEnd:play()
  musicManager.stop()
  appState.set_currentState(SCREEN_END)
end

--- le joueur gagne la partie
function winGame ()
  logMessage("Partie gagnée")
  player.set_hasWon(true)
  musicWin:play()
  musicManager.stop()
  appState.set_currentState(SCREEN_END)
end

--- Quitter l'application
function quitGame ()
  debugMessage("Quitter l'application")
  player.clean()
  musicManager.clean()
  HUD.clean()
  appState.clean()
  viewport.clean()
  love.event.push('quit')
end