## GCB PlatFormer

### Description
Un prototype de jeu de plate-forme.  

===
### Objectifs
Collecter toutes les pièces pour ouvrir la porte et passer au niveau suivant. 

### Mouvements et actions du joueur
Déplacer le joueur à droite et à gauche: touches Q et D ou bien les flèches droite et gauche du clavier.  
Sauter ou Monter à une échelle: touches Z ou bien la flèche haut du clavier.  
Descendre d'une échelle: touches S ou bien la flèche bas du clavier.  
Action: clic gauche.  
Mettre en pause: touche P.  
Relancer la partie: touche R.  
Musique précédente: touche F1.  
Musique suivante: touche F2.  
Quitter le jeu: clic sur la croix ou touche escape.  

#### en mode debug uniquement  
Activer les déplacements avec le clavier (ON/OFF): touche F10.  
Activer les déplacements avec la souris (ON/OFF): touche F11.  
Confiner la souris dans la fenêtre (ON/OFF): touche F12.  
Prochain Niveau: touche + (pavé numérique).  
Perdre une vie: touche - (pavé numérique).  
Perdre la partie: touche Fin.  
Gagner la partie: touche Debut.  

### Interactions
Le joueur:  
- ne peut pas sortir de la zone de jeu.  
- entre en contact avec les éléments de décors.  
- peut monter ou descendre à une échelle
- peut collecter des objets.  
- peut être touché par un PNJ.    

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
Prototype of a platform game.  

===
### Goals
Collect all the coins to open the door and go to the next level.  

### Movements and actions of the player
Move the player to the right and left: keys Q and D or the right and left arrows of the keyboard.
Jump or climb up a ladder: Z key or the top arrow on the keyboard.
Climb down a ladder: S key or the keyboard down arrow.
Action: left click
Pause: P key.  
Restart the game: R key.  
Previous music: F1 key.  
Next music: F2 key.  
Exit the game: click on the cross or escape key.  

### debug mode only
Enable the movements with the keyboard (ON/OFF): F10 key.  
Enable the movements with the mouse (ON/OFF): F11 key.  
Confining the mouse in the window (ON/OFF): F12 key.  
Next Level: + key (keypad).  
Lose a life: - key (keypad).  
Lose the game: End key.  
Win the game: Home key.  

### Interactions
The player:  
- can not get out of the game area.  
- comes into contact with the decorative elements.  
- can go up or down on a ladder.  
- can collect objects.  
- can be touched by an NPC.  

### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)