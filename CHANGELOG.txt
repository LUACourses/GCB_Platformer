# v1.10.2.0.0
## 25/09/2017

1. [](#improved)
	* integration of @Love2D_Skeleton v1.2.0
    (Add Continuous Collision detection to sprites (avoid losing collision detection when Lag is present) )

# v1.10.1.2.0
## 22/09/2017

1. [](#improved)
    * first public release
    * add PNJ
    * finalize level and game management

# v0.10.1.2.0
## 22/09/2017

1. [](#improved)
    * integration of @Love2D_Skeleton v1.1.2

# v0.10.1.1.0
## 21/09/2017

1. [](#improved)
    * integration of @Love2D_Skeleton v1.1.1
    
# v0.10.1.0.0
## 21/09/2017

1. [](#improved)
    * integration of @Love2D_Skeleton v1.1.0

# v0.10.0.1
## 21/09/2017

1. [](#improved)
    * found better values to a natural mouvements on tiles map

1. [](#bugfix)
    * player movement
		* settings.lua : fix typo on param name

# v0.10.0
## 21/09/2017

1. [](#improved)
    * integration of @Love2D_Skeleton v0.1.0 (WIP)
    * hudge refactoring
    * BUG: bad values for player movements

# v0.9.0
## 16/09/2017

1. [](#improved)
    * integration of @Love2D_Skeleton v0.9.0 (WIP)

# v0.5.0
## 12-16/09/2017

1. [](#improved)
    * placing of game elements

# v0.1.0
## 12/09/2017

1. [](#new)
    * ChangeLog started...